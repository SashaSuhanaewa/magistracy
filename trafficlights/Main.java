package trafficlights;

import java.util.Scanner;

import javax.swing.SwingUtilities;

public class Main {
	public static void main(String[] args) {
		System.out.println("������� ���������� ����������: ");
		Scanner scan = new Scanner(System.in);
		int n = scan.nextInt();
		scan.close();
		
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new MainView(n);
			}
		});
	}
}
