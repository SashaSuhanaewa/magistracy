package trafficlights;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class DelayWindow extends JFrame{
	private MainView view;
	private JTextField[] textFields;
	
	public DelayWindow(MainView view, int trafficLightCount) {
		super("Delay Settings");
		this.view = view;
		setLocation(400, 400);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLayout(new BorderLayout());
		
		JButton button = new JButton("���������� ��������");
		button.setBorder(BorderFactory.createEtchedBorder());
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int[] temp = new int[textFields.length];
				for (int i = 0; i < temp.length; i++) {
					double d = new Double(textFields[i].getText()) * 1000;
					temp[i] = (int) d;
					//view.setDelays(temp);
				}
			}
		});
		add(BorderLayout.SOUTH, button);
		
		JPanel settingPanel = new JPanel(new GridLayout(trafficLightCount, 2));
		settingPanel.setBorder(BorderFactory.createEtchedBorder());
			textFields = new JTextField[trafficLightCount];
			for (int i = 0; i < textFields.length; i++) {
				settingPanel.add(createLabel(i + 1));
				settingPanel.add(createTextField(i));
			}
		add(BorderLayout.CENTER, settingPanel);

		pack();
		setVisible(true);
	}
	
	private JLabel createLabel(int number) {
		JLabel res = new JLabel(number + "-� ��������");
		res.setFont(new Font(res.getFont().getFontName(), res.getFont()
				.getStyle(), 14));
		return res;
	}
	
	private JTextField createTextField(int number) {
		JTextField res = new JTextField(view.getLights()[number].getDelay());
		res.setFont(new Font(res.getFont().getFontName(), res.getFont()
				.getStyle(), 14));
		return res;
	}
}