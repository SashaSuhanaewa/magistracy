package trafficlights;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class MainView extends JFrame implements ActionListener {
	private TrafficLight[] lights;
	private int[] delays;
	private long allAreGreenTime;
	private Thread timerThread = null;
	
	private JPanel lightPanel;
	private JPanel buttonPanel;
	private JButton start;
	private JButton stop;
	private JLabel timer;
	
	public MainView(int trafficLightCount) {
		super("Traffic Light");
		
		if (trafficLightCount <= 0) {
			throw new IllegalArgumentException(" ���������� ��������� ���������� ���������� - 1! ");
		}
		if (trafficLightCount > 8) {
			throw new IllegalArgumentException(" ���������� ���������� �� ����� ��������� 8! ");
		}
		
		setSize(680, 420);
		setLocation(200, 200);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
		
		int temp = trafficLightCount > 2 ? 2 : 1;
		lights = new TrafficLight[trafficLightCount];
		lightPanel = new JPanel(new GridLayout(temp, 8 / temp));
			for(int i = 0; i < trafficLightCount; i++) {
				delays = new int[trafficLightCount];
				setDelays();
				lights[i] = new TrafficLight(delays[i]);
				System.out.println(delays[i]);
				lightPanel.add(lights[i]);
			}
			allAreGreenTime = computeGreenTime();
		this.add(BorderLayout.CENTER, lightPanel);
		
		JPanel bottomPanel = new JPanel(new GridLayout(2, 1));
			timer = new JLabel("");
			timer.setBorder(BorderFactory.createEtchedBorder());
			timer.setFont(new Font(timer.getFont().getFontName(), timer.getFont().getStyle(), 16));
			bottomPanel.add(timer);
			
			buttonPanel = new JPanel(new GridLayout(1, 2));
				start = new JButton("Start");
				start.addActionListener(this);
				buttonPanel.add(start);
				
				stop = new JButton("Stop");
					stop.addActionListener(this);
				buttonPanel.add(stop);
			bottomPanel.add(buttonPanel);	
		this.add(BorderLayout.SOUTH, bottomPanel);
		
		//pack();
		setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent ae) {
		if (ae.getActionCommand().equals("Start")) {
			for (TrafficLight t : lights) {
				t.start();
			}
			timerThread = new Thread (new Runnable() {
				@Override
				public void run() {
					long timeLeft = allAreGreenTime;
					System.out.println(allAreGreenTime);
					while (true) {
						timer.setText("�� ������� ��������� ������� ������� �� ���� ���������� ������c�: " + 
											(timeLeft / 1000) + " ���");
						int wait = 1000 + (int)(timeLeft % 1000);
						try {
							Thread.sleep(wait);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						if (timeLeft <= 0) {
							timeLeft = allAreGreenTime;
						}
						timeLeft -= wait;
					}
				}
			});
			timerThread.start();
		} else {
			for (TrafficLight t : lights) {
				t.stop();
			}	
			timerThread.stop();
			timer.setText("");
		}
	}
	
	public TrafficLight[] getLights() {
		return lights;
	}
	
	public void setDelays() {
		Random rand = new Random();
		for (int i = 0; i < delays.length; i++) {
			delays[i] = (int)((rand.nextDouble() + 0.05) * 10000);
		}
	}
	
	public long computeGreenTime() {
		long res = 1;
		for (TrafficLight t : lights) {
			res = nok(res, 2 * t.getDelay() - t.getSmallDelay() + 6000);
		}
		return res;
	}
	
	long nod(long a, long b) {  // ���� ��� ���� �����
	    long tmp;
	    while(a != 0 && b != 0) {
	        a %= b;
	        tmp = a;
	        a = b;
	        b = tmp;
	    }
	    return a + b;
	}
	
	private long nok(long a, long b) { // ���� ��� ���� �����
	    return a / nod(a, b) * b;
	}
}