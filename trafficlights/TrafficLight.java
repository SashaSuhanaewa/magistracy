package trafficlights;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class TrafficLight extends JPanel {
	private Thread launcher;
	private int delay = 10000;
	private int smallDelay = 2000;
	private boolean isStoped = false;
	
	JPanel lightPanel;
	JLabel redLabel;
	JLabel yellowLabel;
	JLabel greenLabel;

	//*** ������������ ***************
	public TrafficLight() {
		super(new GridLayout(3, 1));
		setBorder(BorderFactory.createLineBorder(Color.BLACK));
		redLabel = createLabel();
		add(redLabel);

		yellowLabel = createLabel();
		add(yellowLabel);

		greenLabel = createLabel();
		add(greenLabel);
	}
	
	public TrafficLight(int delay) {
		this();
		this.delay = delay;
		this.smallDelay = delay / 5;
	}
	//********************************
	
	private JLabel createLabel() {
		JLabel temp = new JLabel("");
		temp.setHorizontalAlignment(JLabel.CENTER);
		temp.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		temp.setFont(new Font(temp.getFont().getFontName(), temp.getFont()
				.getStyle(), 16));
		return temp;
	}

	private void setLabelState(JLabel label, String text, Color c) {
		label.setText(text);
		label.setForeground(c);
	}
	
	@SuppressWarnings("static-access")
	private void blink(JLabel label, String text, Color c) {
		try {
			setLabelState(label, "", Color.gray);
			Thread.currentThread().sleep(500);
			
			setLabelState(label, text, c);
			Thread.currentThread().sleep(500);
			
			setLabelState(label, "", Color.gray);
			Thread.currentThread().sleep(500);
		
			setLabelState(label, text, c);
			Thread.currentThread().sleep(500);
			
			setLabelState(label, "", Color.gray);
			Thread.currentThread().sleep(500);
			
			setLabelState(label, text, c);
			Thread.currentThread().sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}

	public void start() {
		launcher = new Thread(new Runnable() {
			@Override
			public void run() {
				go();
			}
		});
		launcher.start();
	}
	
	@SuppressWarnings("deprecation")
	public void stop() {
		launcher.stop();
		setLabelState(redLabel, "", Color.gray);
		setLabelState(yellowLabel, "", Color.gray);
		setLabelState(greenLabel, "", Color.gray);
	}
	
	@SuppressWarnings("static-access")
	public void go() {
		isStoped = false;
		while (!isStoped) {
			try {
				setLabelState(redLabel, "�������", Color.red);
				setLabelState(yellowLabel, "", Color.gray);
				Thread.currentThread().sleep(delay - 2 * smallDelay);

				blink(redLabel, "�������", Color.red);
				
				setLabelState(yellowLabel, "ƨ����", Color.yellow);
				Thread.currentThread().sleep(smallDelay);

				setLabelState(greenLabel, "��˨���", Color.green);
				setLabelState(redLabel, "", Color.gray);
				setLabelState(yellowLabel, "", Color.gray);
				Thread.currentThread().sleep(delay - smallDelay);
				
				blink(greenLabel, "��˨���", Color.green);

				setLabelState(greenLabel, "", Color.gray);
				setLabelState(yellowLabel, "ƨ����", Color.yellow);
				Thread.currentThread().sleep(smallDelay);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public int getDelay() {
		return delay;
	}
	
	public int getSmallDelay() {
		return smallDelay;
	}
}
