/**
 * Created by Максим on 25.10.2014.
 */
import javax.swing.JOptionPane;


public class Main {
    public static void main(String[] args){
        String[] possibleValues = {"9x9", "16x16"};
        String selectedValue = (String) JOptionPane.showInputDialog(null, "�������� ������ ����: ",
                "�����",
                JOptionPane.OK_CANCEL_OPTION, null, possibleValues, possibleValues[0]);

        if(selectedValue == "9x9"){
            new Saper(9, 9, 10);
        }

        if(selectedValue == "16x16"){
            new Saper(16, 16, 40);
        }
    }
}
