package ru.kfu.itis.gr3.Tsigankov;

import java.io.BufferedReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public abstract class Tools {

	private static int getIntNumberInInterval(int a1, int a2) {
		Scanner sc = new Scanner(System.in);
		int returningNumber = 0;
		boolean b = true;
		while (b) {
			try {
				returningNumber = sc.nextInt();
				if (returningNumber < a1 || returningNumber > a2) {
					throw new InputMismatchException();
				}
				b = false;
			} catch (InputMismatchException e) {
				System.out.println("����������, ������� ����� �� " + a1
						+ " �� " + a2 + "!");
				sc = new Scanner(System.in);

			}

		}
		
		return returningNumber;
	}

	public static int outputMenu() {
		System.out.println("����� �������� �� ������ ���������?");
		System.out.println("1 - ��������� ������� ������ �� �����");
		System.out
				.println("2 - �������� ������� � ������ (������� ������, ���� ��� �� ����");
		System.out.println("3 - ������� ������� �� ������");
		System.out
				.println("4 - ������� ���� � �������� ������ (����� �������)");
		System.out.println("5 - ������� ������ �� �������");
		System.out
				.println("6 - ������� ������ � ���� output.txt � �������� �������� ���������");
		System.out.println("7 - �������� ������");
		System.out.println("8 - ������� ����� ������ ���������");
		System.out.println("9 - �����");

		int returningNumber = getIntNumberInInterval(1, 9);

		return returningNumber;

	}

	public static boolean doChoicedOperation(Tree tree, int num)
			throws IOException {
		boolean b = false;
		switch (num) {
		case 1:
			getTreeFromFile(tree);
			break;
		case 2:
			insertElementInThe(tree);
			break;
		case 3:
			deleteElementFromThe(tree);
			break;
		case 4:
			findElementInThe(tree);
			break;
		case 5:
			print(tree); // ������������ �������� �������, ������ ����������
							// �������, �� ��������� ��� ����� ��������
			break;
		case 6:
			printInOutputDotTxt(tree);
			break;
		case 7:
			clearThe(tree);
			break;
		case 8:
			findSumOfEvenNumbersInThe(tree);
			break;
		case 9:
			b = exitOperation(tree);
			break;
		default:
			System.out.println("������ �����");

		}
		return b;

	}

	private static void getTreeFromFile(Tree tree) {
		int answer = 1;
		if (!tree.isEmpty()) {
			System.out
					.println("��������, ������ � ������ ������ ����� �������, ����������?");
			System.out.println("1 - ��");
			System.out.println("2 - ���");
			answer = getIntNumberInInterval(1, 2);

		}
		if (answer == 1) {
			System.out
					.println("�� ������ ����� �� ������ ��������� ������? (��� ����� ����������� � �������� �������� ���������)");
			System.out.println("1 - input1.txt");
			System.out.println("2 - input2.txt");
			System.out.println("3 - input3.txt");
			answer = getIntNumberInInterval(1, 3);
			File file;
			try {
				switch (answer) {
				case 1:
					file = new File("input1.txt");
					break;
				case 2:
					file = new File("input2.txt");
					break;
				case 3:
					file = new File("input3.txt");
					break;
				default:
					file = new File("input1.txt");

				}
				BufferedReader fReader = new BufferedReader(
						new FileReader(file));
				String str = fReader.readLine();
				while (str != null) {
					int num = Integer.parseInt(str);
					tree.insertTheRoot(num);

					str = fReader.readLine();
				}
				fReader.close();

			} catch (FileNotFoundException e) {
				System.out.println("���� �� ������!");
			} catch (InputMismatchException | NumberFormatException
					| IOException e) {
				System.out.println("���� ���������!");
			}

		}
	}

	private static void insertElementInThe(Tree tree) {
		System.out.println("������� �����");
		Scanner sc = new Scanner(System.in);
		int num = 0;
		boolean b = true;
		while (b) {
			try {
				num = sc.nextInt();

				b = false;
			} catch (InputMismatchException e) {
				System.out.println("����������, ������� �����!");
				sc = new Scanner(System.in);

			}

		}
		
		tree.insertTheRoot(num);
	}

	private static void deleteElementFromThe(Tree tree) {
		System.out.println("������� �����");
		Scanner sc = new Scanner(System.in);
		int num = 0;
		boolean b = true;
		while (b) {
			try {
				num = sc.nextInt();

				b = false;
			} catch (InputMismatchException e) {
				System.out.println("����������, ������� �����!");
				sc = new Scanner(System.in);

			}

		}
		
		tree.deleteElementById(num);
	}

	private static void findElementInThe(Tree tree) {
		System.out.println("������� �����");
		Scanner sc = new Scanner(System.in);
		int num = 0;
		boolean b = true;
		while (b) {
			try {
				num = sc.nextInt();

				b = false;
			} catch (InputMismatchException e) {
				System.out.println("����������, ������� �����!");
				sc = new Scanner(System.in);

			}

		}
		String path = tree.findPathOfRootById(num);
		if (path != "-") {
			System.out.println("���� �������� � ������� " + num + " - " + path);
		}
		
	}

	private static void print(Tree tree) {
		tree.print();
	}

	private static void printInOutputDotTxt(Tree tree) {
		File file = new File("output.txt");
		tree.printInFile(file);

	}

	private static void clearThe(Tree tree) {
		System.out.println("�� �������?");
		System.out.println("1 - ��");
		System.out.println("2 - ���");
		int answer = getIntNumberInInterval(1, 2);
		if (answer == 1) {
			tree.clear();
			;
			System.out.println("������ �������");
		} else {
			System.out.println("������ �� �������");
		}
	}

	private static void findSumOfEvenNumbersInThe(Tree tree) {
		System.out.println(tree.assumeEvenNumbersSum());
	}

	private static boolean exitOperation(Tree tree) throws IOException {
		boolean b = false;
		System.out.println("�� �������, ��� ������ �����?");
		System.out.println("1 - ��");
		System.out.println("2 - ���");
		int answer = getIntNumberInInterval(1, 2);
		File file = new File("output.txt");
		if (!file.exists()) {
			file.createNewFile();
		}

		if (answer == 1) {
			b = true;
			printInOutputDotTxt(tree);
		}
		return b;

	}
}
