package ru.kfu.itis.gr3.Tsigankov;

public class RootOfTreeStack {
	private RootOfTreeStackElement toFirst;
	
	private int numberOfElements;

	public RootOfTreeStack() {
		toFirst = null;
		
		numberOfElements = 0;
	}

	

	public void push(RootOfTree rootOfTree) {
			
			RootOfTreeStackElement insertingRoot = new RootOfTreeStackElement(rootOfTree, this.toFirst);
			this.numberOfElements++;
			this.toFirst = insertingRoot;

		
	}

	public boolean stackIsEmpty() {
		return (this.numberOfElements == 0) ? true : false;
	}

	public RootOfTree pop() {
		RootOfTree returning = null;
		if (!stackIsEmpty()) {
			returning = this.toFirst.getThis();
			this.toFirst = this.toFirst.getNext();
			this.numberOfElements--;
		}
		return returning;
	}

}
