package ru.kfu.itis.gr3.Tsigankov;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Tree {
	private RootOfTree root;

	public boolean isEmpty() {
		return (this.root == null) ? true : false;
	}
	
	public void clear(){
		this.root = null;
	}

	public Tree() {
		root = null;
	}

	public Tree(int num) {
		this.root = new RootOfTree(num);
	}

	public RootOfTree findRootById(int id) {
		RootOfTree help = this.root;
		if (this.root != null) {

			boolean b = true;

			while (help.getId() != id && b) {
				if (id < help.getId()) {
					if (help.getLeft() != null) {
						help = help.getLeft();
					} else {
						b = false;
					}
				} else {
					if (help.getRight() != null) {
						help = help.getRight();
					} else {
						b = false;
					}
				}
			}

			if (help.getId() != id) {
				help = null;
				// System.out.println("�������� ���!");
			}

		} else {
			// System.out.println("������ �����!");
			help = null;
		}

		return help;
	}

	//������� ���� �������� (��. 15 - ����� ������ ������)
	public String findPathOfRootById(int id) {
		RootOfTree help = this.root;
		String str = "������";
		if (this.root != null) {

			boolean b = true;
			
			while (help.getId() != id && b) {
				if (id < help.getId()) {
					if (help.getLeft() != null) {
						str += " �����";
						help = help.getLeft();
					} else {
						b = false;
					}
				} else {
					if (help.getRight() != null) {
						str += " ������";
						help = help.getRight();
					} else {
						b = false;
					}
				}
			}

			if (help.getId() != id) {

				System.out.println("�������� ���!");
				str = "-";
			}

		} else {
			System.out.println("������ �����!");
			str = "-";
		}

		return str;
	}

	//����� ��� ������ �� ������� ����, �� ����������� ������ 
	private void print(String spaces, RootOfTree rootoftree) {
		if (rootoftree != null) {
			System.out.println(spaces + rootoftree.getId());
			print(spaces + " ", rootoftree.getLeft());
			print(spaces + " ", rootoftree.getRight());
		}
	}
	
	
	//����� ��� ������ �� ������� ����, ����������� ������. ���������� �������� ���������� �����
	public void print() {
		if (this.root != null) {
			System.out.println(this.root.getId());
			print(" " , this.root.getLeft());
			print(" ", this.root.getRight());
		} else {
			System.out.println("������ �����!");
		}
	}
	
	
	public void insertTheRoot(int num) {
		if (this.findRootById(num) == null) {
			if (this.root == null) {
				this.root = new RootOfTree(num);
			} else {
				RootOfTreeStack stack = new RootOfTreeStack();
				RootOfTree help = this.root;
				boolean b = true;
				// System.out.println("����");
				while (b) {

					stack.push(help);
					if (num < help.getId()) {
						// System.out.println(help.getId());
						if (help.getLeft() != null) {
							help = help.getLeft();
						} else {
							help.setLeft(new RootOfTree(num));
							b = false;
						}

					} else {
						// System.out.println(help.getId());
						if (help.getRight() != null) {

							help = help.getRight();
						} else {
							help.setRight(new RootOfTree(num));
							b = false;
						}

					}

				}
				// System.out.println("������������");
				balancing(stack);
				// System.out.println("��������������");
			}
		} else {
			System.out.println("������� ��� ���� � ������!");
		}
	}
	
	//����� ��� ������������ 
	private void balancing(RootOfTreeStack stack) {

		boolean b = true;
		RootOfTree help = null;

		while (!stack.stackIsEmpty() && b) {
			help = stack.pop();
			// System.out.println(help.getId() + " " + help.getLeftDeep() + " "
			// + help.getRightDeep());
			if (Math.abs(help.getRightDeep() - help.getLeftDeep()) > 1) {
				// System.out.println("!");
				b = false;
			} else {
				help.setDeep(Math.max(help.getRightDeep(), help.getLeftDeep()) + 1);
			}

		}

		RootOfTree pointerOnHelp = stack.pop();
		if (!b) {
			// ��������-���������
			if (help.getRightDeep() > help.getLeftDeep()) {
				if (help.getRight().getLeftDeep() <= help.getRight()
						.getRightDeep()) {
					// System.out.println("��");
					// this.print();
					// System.out.println(help.getLeft().getLeftDeep() + );
					littleLeftRotate(help, pointerOnHelp);
				} else {
					// System.out.println("��");
					bigLeftRotate(help, pointerOnHelp);
				}
			} else {
				if (help.getLeft().getRightDeep() <= help.getLeft()
						.getLeftDeep()) {
					// System.out.println("��");
					littleRightRotate(help, pointerOnHelp);
				} else {
					// System.out.println("��");
					// this.print();
					bigRightRotate(help, pointerOnHelp);
				}
			}
		}

		if (pointerOnHelp != null) {
			pointerOnHelp.setDeep(Math.max(pointerOnHelp.getLeftDeep(),
					pointerOnHelp.getRightDeep()) + 1);
			while (!stack.stackIsEmpty()) {
				pointerOnHelp = stack.pop();
				pointerOnHelp.setDeep(Math.max(pointerOnHelp.getLeftDeep(),
						pointerOnHelp.getRightDeep()) + 1);
			}
		}

		// print();

	}

	//��������
	private void littleLeftRotate(RootOfTree a, RootOfTree pointerToA) {
		RootOfTree secondRoot = a.getRight().getLeft();

		if (pointerToA != null) {

			if (pointerToA.getLeft() == a) {
				a.getRight().setLeft(a);
				pointerToA.setLeft(a.getRight());
				a.setRight(secondRoot);

				a.setDeep(Math.max(a.getLeftDeep(), a.getRightDeep()) + 1);
				pointerToA.getLeft().setDeep(
						Math.max(pointerToA.getLeft().getLeftDeep(), pointerToA
								.getLeft().getLeftDeep()) + 1);

			} else {
				a.getRight().setLeft(a);
				pointerToA.setRight(a.getRight());
				a.setRight(secondRoot);

				a.setDeep(Math.max(a.getLeftDeep(), a.getRightDeep()) + 1);
				pointerToA.getRight().setDeep(
						Math.max(pointerToA.getRight().getLeftDeep(),
								pointerToA.getRight().getLeftDeep()) + 1);
			}
		} else {
			a.getRight().setLeft(a);
			this.root = a.getRight();
			a.setRight(secondRoot);

			a.setDeep(Math.max(a.getLeftDeep(), a.getRightDeep()) + 1);
			this.root.setDeep(Math.max(this.root.getLeftDeep(),
					this.root.getLeftDeep()) + 1);
		}
	}

	private void bigLeftRotate(RootOfTree a, RootOfTree pointerToA) {
		RootOfTree thirdRoot = a.getRight().getLeft().getLeft();
		RootOfTree fourthRoot = a.getRight().getLeft().getRight();

		if (pointerToA != null) {

			if (pointerToA.getLeft() == a) {
				a.getRight().getLeft().setLeft(a);
				a.getRight().getLeft().setRight(a.getRight());
				pointerToA.setLeft(a.getRight().getLeft());
				a.setRight(thirdRoot);
				pointerToA.getLeft().getRight().setLeft(fourthRoot);

				a.setDeep(Math.max(a.getLeftDeep(), a.getRightDeep()) + 1);
				pointerToA
						.getLeft()
						.getRight()
						.setDeep(
								Math.max(pointerToA.getLeft().getRight()
										.getLeftDeep(), pointerToA.getLeft()
										.getRight().getRightDeep()) + 1);
				pointerToA.getLeft().setDeep(
						Math.max(pointerToA.getLeft().getLeftDeep(), pointerToA
								.getLeft().getRightDeep()) + 1);

			} else {
				a.getRight().getLeft().setLeft(a);
				a.getRight().getLeft().setRight(a.getRight());
				pointerToA.setRight(a.getRight().getLeft());
				a.setRight(thirdRoot);
				pointerToA.getRight().getRight().setLeft(fourthRoot);

				a.setDeep(Math.max(a.getLeftDeep(), a.getRightDeep()) + 1);
				pointerToA
						.getRight()
						.getRight()
						.setDeep(
								Math.max(pointerToA.getRight().getRight()
										.getLeftDeep(), pointerToA.getRight()
										.getRight().getRightDeep()) + 1);
				pointerToA.getRight().setDeep(
						Math.max(pointerToA.getRight().getLeftDeep(),
								pointerToA.getRight().getRightDeep()) + 1);
			}
		} else {
			a.getRight().getLeft().setLeft(a);
			a.getRight().getLeft().setRight(a.getRight());
			this.root = (a.getRight().getLeft());
			a.setRight(thirdRoot);
			this.root.getRight().setLeft(fourthRoot);

			a.setDeep(Math.max(a.getLeftDeep(), a.getRightDeep()) + 1);
			this.root.getRight().setDeep(
					Math.max(this.root.getRight().getLeftDeep(), this.root
							.getRight().getRightDeep()) + 1);
			this.root.setDeep(Math.max(this.root.getLeftDeep(),
					this.root.getRightDeep()) + 1);
		}
	}

	private void littleRightRotate(RootOfTree a, RootOfTree pointerToA) {
		RootOfTree secondRoot = a.getLeft().getRight();

		if (pointerToA != null) {

			if (pointerToA.getLeft() == a) {
				a.getLeft().setRight(a);
				pointerToA.setLeft(a.getLeft());
				a.setLeft(secondRoot);

				a.setDeep(Math.max(a.getLeftDeep(), a.getRightDeep()) + 1);
				pointerToA.getLeft().setDeep(
						Math.max(pointerToA.getLeft().getLeftDeep(), pointerToA
								.getLeft().getLeftDeep()) + 1);

			} else {
				a.getLeft().setRight(a);
				pointerToA.setRight(a.getLeft());
				a.setLeft(secondRoot);

				a.setDeep(Math.max(a.getLeftDeep(), a.getRightDeep()) + 1);
				pointerToA.getRight().setDeep(
						Math.max(pointerToA.getRight().getLeftDeep(),
								pointerToA.getRight().getLeftDeep()) + 1);
			}
		} else {
			a.getLeft().setRight(a);
			this.root = (a.getLeft());
			a.setLeft(secondRoot);

			a.setDeep(Math.max(a.getLeftDeep(), a.getRightDeep()) + 1);
			this.root.setDeep(Math.max(this.root.getLeftDeep(),
					this.root.getLeftDeep()) + 1);
		}
	}

	private void bigRightRotate(RootOfTree a, RootOfTree pointerToA) {

		RootOfTree thirdRoot = a.getLeft().getRight().getRight();
		RootOfTree fourthRoot = a.getLeft().getRight().getLeft();

		if (pointerToA != null) {

			if (pointerToA.getLeft() == a) {
				a.getLeft().getRight().setRight(a);
				a.getLeft().getRight().setLeft(a.getLeft());
				pointerToA.setLeft(a.getLeft().getRight());
				a.setLeft(thirdRoot);
				pointerToA.getLeft().getLeft().setRight(fourthRoot);

				a.setDeep(Math.max(a.getLeftDeep(), a.getRightDeep()) + 1);
				pointerToA
						.getLeft()
						.getLeft()
						.setDeep(
								Math.max(pointerToA.getLeft().getLeft()
										.getLeftDeep(), pointerToA.getLeft()
										.getLeft().getRightDeep()) + 1);
				pointerToA.getLeft().setDeep(
						Math.max(pointerToA.getLeft().getLeftDeep(), pointerToA
								.getLeft().getRightDeep()) + 1);

			} else {
				a.getLeft().getRight().setRight(a);
				a.getLeft().getRight().setLeft(a.getLeft());
				pointerToA.setRight(a.getLeft().getRight());
				a.setLeft(thirdRoot);
				pointerToA.getRight().getLeft().setRight(fourthRoot);

				a.setDeep(Math.max(a.getLeftDeep(), a.getRightDeep()) + 1);
				pointerToA
						.getRight()
						.getLeft()
						.setDeep(
								Math.max(pointerToA.getRight().getLeft()
										.getLeftDeep(), pointerToA.getRight()
										.getLeft().getRightDeep()) + 1);
				pointerToA.getRight().setDeep(
						Math.max(pointerToA.getRight().getLeftDeep(),
								pointerToA.getRight().getRightDeep()) + 1);
			}
		} else {
			a.getLeft().getRight().setRight(a);
			a.getLeft().getRight().setLeft(a.getLeft());
			this.root = (a.getLeft().getRight());
			a.setLeft(thirdRoot);
			this.root.getLeft().setRight(fourthRoot);

			a.setDeep(Math.max(a.getLeftDeep(), a.getRightDeep()) + 1);
			this.root.getLeft().setDeep(
					Math.max(this.root.getLeft().getLeftDeep(), this.root
							.getLeft().getRightDeep()) + 1);
			this.root.setDeep(Math.max(this.root.getLeftDeep(),
					this.root.getRightDeep()) + 1);
		}
	}

	//��������
	public void deleteElementById(int num) {
		if (this.root == null) {
			System.out.println("������ ������");
		} else {
			RootOfTreeStack stack = new RootOfTreeStack();
			RootOfTree help = this.root;
			if (help.getId() == num) {

				stack.push(help);
				if (help.getLeft() == null) {

					if (help.getRight() == null) {

						this.root = null;
						// help = null;

					} else {
						this.root = this.root.getRight();

					}
				} else {
					if (help.getRight() == null) {
						this.root = this.root.getRight();

					} else {

						findMinimalRightElementWithEmptyLeftChild(stack, help);
					}
				}

			} else {
				boolean b = true;
				while (b) {

					if (help.getId() == num) {
						b = false;
					} else {
						stack.push(help);
						if (num < help.getId()) {
							if (help.getLeft() != null) {
								help = help.getLeft();
							} else {
								System.out.println("�������� ���");
								b = false;

							}

						} else {
							if (help.getRight() != null) {

								help = help.getRight();
							} else {
								System.out.println("�������� ���");
								b = false;

							}

						}

					}

				}

				if (help.getId() == num) {
					RootOfTree toHelp = stack.pop();
					stack.push(toHelp);
					if (help.getLeft() == null) {

						if (help.getRight() == null) {

							if (toHelp.getLeft() == help) {
								toHelp.setLeft(null);

							} else {
								toHelp.setRight(null);
							}
							// help = null;

						} else {
							if (toHelp.getLeft() == help) {
								toHelp.setLeft(help.getRight());

							} else {
								toHelp.setRight(help.getRight());
							}

						}
					} else {
						if (help.getRight() == null) {
							if (toHelp.getLeft() == help) {
								toHelp.setLeft(help.getLeft());

							} else {
								toHelp.setRight(help.getLeft());
							}

						} else {

							findMinimalRightElementWithEmptyLeftChild(stack,
									help);
						}
					}
				}
			}
			balancing(stack);
		}
	}

	
	//����� ��� ���������� ������������ �������� � ������ ���������
	private void findMinimalRightElementWithEmptyLeftChild(
			RootOfTreeStack stack, RootOfTree deletingElement) {
		stack.push(deletingElement);
		RootOfTree help = deletingElement.getRight();
		while (help.getLeft() != null) {
			stack.push(help);
			help = help.getLeft();
		}
		deletingElement.setIdFrom(help);
		RootOfTree toHelp = stack.pop();
		stack.push(toHelp);
		if (toHelp.getLeft() == help) {
			toHelp.setLeft(help.getRight());
		} else {
			toHelp.setRight(help.getRight());
		}

	}
	
	
	//���������� ������� ����� ������ ����� 
	public int assumeEvenNumbersSum(RootOfTree rootOfTree, int sum) {
		if (rootOfTree != null) {
			if (rootOfTree.getId() % 2 == 0) {
				sum += rootOfTree.getId();
			}
			sum = assumeEvenNumbersSum(rootOfTree.getLeft(), sum);
			sum = assumeEvenNumbersSum(rootOfTree.getRight(), sum);
		}
		return sum;
	}

	//������� ����� ������ ����� 
	public int assumeEvenNumbersSum() {
		int sum = 0;
		if (this.root != null) {
			if (this.root.getId() % 2 == 0) {
				sum += this.root.getId();
			}
			sum = assumeEvenNumbersSum(this.root.getLeft(), sum);
			sum = assumeEvenNumbersSum(this.root.getRight(), sum);
		} else {
			System.out.println("������ �����!");
			sum = -1;
		}
		return sum;
	}

	//������ � ����
	public void printInFile(File file) {
		try {

			if (!file.exists()) {
				file.createNewFile();
			}
			FileWriter fWriter = new FileWriter(file);
			//System.out.println("������");
			if (this.root != null) {
				
				fWriter.write(this.root.getId() + "\r\n");
				//System.out.println("��������");
				printInFile(fWriter, " ", this.root.getLeft());
				printInFile(fWriter, " ", this.root.getRight());
				fWriter.write("����� ������ ��������� - "
						+ this.assumeEvenNumbersSum());
				//System.out.println("!");
			} else {
				//System.out.println("!");
				fWriter.write("������ �����!");
				//System.out.println("!");
			}
			fWriter.close();
		} catch (IOException e) {
			System.out.println("������ �����/������");
		}
	}

	private void printInFile(FileWriter fWriter, String spaces,
			RootOfTree rootoftree) throws IOException {
		if (rootoftree != null) {
			
			fWriter.write(spaces + rootoftree.getId() + "\r\n");
			printInFile(fWriter, spaces + " ", rootoftree.getLeft());
			printInFile(fWriter, spaces + " ", rootoftree.getRight());
		}
	}
}
