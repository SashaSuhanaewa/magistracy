package ru.kfu.itis.gr3.Tsigankov;


public class RootOfTree {
	private RootOfTree left;
	private RootOfTree right;
	private int id;
	private int maxDeep;
	public RootOfTree(int num) {
		// TODO Auto-generated constructor stub
		this.left = null;
		this.right = null;
		this.id = num;
	}
	
	public void setIdFrom(RootOfTree copyingObject){
		this.id = copyingObject.getId();
	}
	
	public int getId(){
		return this.id;
	}
	
	public RootOfTree getLeft(){
		return this.left;
	}
	
	public RootOfTree getRight(){
		return this.right;
	}
	
	public void setLeft(RootOfTree rootOfTree){
		this.left = rootOfTree;
	}
	
	public void setRight(RootOfTree rootOfTree){
		this.right = rootOfTree;
	}
	
	//������ ��� ������ � ��������
	public void setDeep(int deepNumber){
		this.maxDeep = deepNumber;
	}
	
	public int getDeep(){
		
		return this.maxDeep;
	}
	
	public int getLeftDeep(){
		return (this.getLeft()==null)?-1:this.getLeft().getDeep();
	}
	
	public int getRightDeep(){
		return (this.getRight()==null)?-1:this.getRight().getDeep();
	}
	
	

}
