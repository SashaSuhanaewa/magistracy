package ru.kfu.itis.gr3.Tsigankov;

public class RootOfTreeStackElement {
	private RootOfTreeStackElement next;
	private RootOfTree thisElement;
	
	public RootOfTreeStackElement() {
		// TODO Auto-generated constructor stub
		thisElement = null;
		next = null;
	}
	
	public RootOfTreeStackElement(RootOfTree root, RootOfTreeStackElement nextElement) {
		// TODO Auto-generated constructor stub
		this.thisElement = root;
		this.next = nextElement;
	}
	
	
	
	public RootOfTreeStackElement getNext(){
		return this.next;
	}
	
	public RootOfTree getThis(){
		return this.thisElement;
	}
}
