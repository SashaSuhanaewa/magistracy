package ru.kpfu.itis.sem3;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.regex.Pattern;

public class Tree1 {
    private List<ArrayList<Integer>> T = new ArrayList<ArrayList<Integer>>();
    private List<ArrayList<Integer>> VS = new ArrayList<ArrayList<Integer>>();
    private int n;

    public static void main(String[] args) {
        Tree1 rt = new Tree1();
        rt.show(rt.read("a.txt"));
        rt.show(rt.frame(rt.addVertices(), rt.edges(rt.read("a.txt"))));
    }

    /**
     * ������� Map �� �������, key - ���� ������, value - ��� ���� ������
     * �������� � ���� ������ � ��������� �� �����
     *
     * @param gr
     * @return HashMap<String, Integer> edges
     */
    public List<Map.Entry<String, Integer>> edges(int[][] gr) {

        Map<String, Integer> edges = new HashMap();
        LinkedList<String> vertices = new LinkedList<String>();
        for (int i = 0; i < n; i++) {
            for (int j = i; j < n; j++) {
                if (gr[i][j] != 0) {
                    vertices.add(i + "," + j);
                    edges.put(i + "," + j, gr[i][j]);
                }
            }
        }
        List<Map.Entry<String, Integer>> entries = new ArrayList<Map.Entry<String, Integer>>(
                edges.entrySet());

        Collections.sort(entries, new Comparator<Map.Entry<String, Integer>>() {
            public int compare(Map.Entry<String, Integer> e1,
                               Map.Entry<String, Integer> e2) {
                int v1 = e1.getValue();
                int v2 = e2.getValue();
                return (v1 > v2) ? 1 : (v1 == v2) ? 0 : -1;
            }
        });

        return entries;
    }

public ArrayList<String> addVertices() {

    List<Map.Entry<String, Integer>> entries = edges(read("a.txt"));
    System.out.println(entries);
        ArrayList<String> T1 = new ArrayList<String>();
        for (int i = 0; i < entries.size(); i++) { // ребра
            T1.add(entries.get(i).getKey());
        }
        ArrayList<String> T = new ArrayList<String>();

        ArrayList<Integer> T0 = new ArrayList<Integer>();

        int j = 0;

        for (int i = 0; i < n; i++) {  // заполняем VS
            ArrayList<Integer> l = new ArrayList<Integer>();
            l.add(i);
            VS.add(l);
        }
        ArrayList E = T1;
        // System.out.println(VS);
        while (VS.size() > 1) {
            int W1 = 0;
            int W2 = 0;
            String vw = (String) E.get(j);
            E.remove(j);
            int v = Integer.parseInt(vw.substring(0, vw.indexOf(",")));
            int w = Integer.parseInt(vw.substring(vw.indexOf(",") + 1));
            for (int i = 0; i < VS.size(); i++) {
                for (int t = 0; t < VS.get(i).size(); t++) {
                    if (v == VS.get(i).get(t)) {
                        W1 = i;
                    }
                    if (w == VS.get(i).get(t)) {
                        W2 = i;
                    }
                }
            }
            if (W1 != W2) {
                VS.get(W1).add(W2);
                VS.remove(W2);
                T.add(v+","+w);
            }
//            System.out.println(VS);
        }
        System.out.println(T);
        return T;
    }

    public int[][] frame(ArrayList<String> T, List<Map.Entry<String, Integer>> e) {

        int p = 0;
        int[][] frame = new int[n][n];
        for (int i = 0; i < T.size(); i++) {
     for (int j = 0; j < e.size(); j++) {
            if (e.get(j).getKey().equals((T.get(i)))) {
                p = j;

            }}
        int v = Integer.parseInt(T.get(i).substring(0,T.get(i).indexOf(",")));
        int w = Integer.parseInt(T.get(i).substring(T.get(i).indexOf(",") + 1));
            frame[v][w] = e.get(p).getValue();
        }
        return frame;
    }

    /**
     * ���������� � ����� � �������� ������� �� int-��- �������� ����
     *
     * @param str
     * @return int[][] gr
     */
    public int[][] read(String str) {
        Scanner sc = null;
        n = 0;
        int[][] gr = null;
        Pattern pat = Pattern.compile(" ");
        try {
            sc = new Scanner(new File(str));
            n = sc.nextInt();
            gr = new int[n][n];
            int i = 0;
            int j = 0;
            while (sc.hasNext()) {
                int arr = sc.nextInt();
                gr[i][j] = arr;
                j++;
                if (j == 7) {
                    j = 0;
                    i++;
                }
            }
        } catch (FileNotFoundException e) {
            System.err.print("���� �� ������");
        }
        return gr;
    }

    public void show(int[][] gr) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(gr[i][j] + "  ");
            }
            System.out.println();
        }
    }
}
