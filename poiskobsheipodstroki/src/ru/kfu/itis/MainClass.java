package ru.kfu.itis;

import java.util.Scanner;

public class MainClass {
	
	public static void printArrayWithStrings(int[][] array, String s1, String s2){
		String s1Help = "  " + s1;
		String s2Help = " " + s2;
		for(int i = 0; i<s1Help.length();i++){
			System.out.print(s1Help.charAt(i) + " ");
		}
		System.out.println();
		for(int i = 0; i<array.length; i++){
			System.out.print(s2Help.charAt(i) + " ");
			for(int j = 0; j<array[i].length; j++){
				System.out.print(array[i][j] + " ");
			}
			System.out.println();
		}
		
	}
	
	public static String findSubString(String s1, String s2){
		int[][] helpMatrix = new int[s2.length()+1][s1.length()+1];
		
		for(int i = 0; i<helpMatrix[0].length; i++){
			helpMatrix[0][i] = 0;
			
		}
		
		for(int i = 0; i<helpMatrix.length; i++){
			helpMatrix[i][0] = 0;
		}
		int maxI = 0;
		int maxJ = 0;
		//�� �������� - s1
		//�� ������� - s2
		for(int i = 1; i<helpMatrix.length;i++){
			for(int j = 1; j<helpMatrix[i].length; j++){
				if(s1.charAt(j-1) == s2.charAt(i-1)){
					helpMatrix[i][j] = helpMatrix[i-1][j-1] + 1;
					if(helpMatrix[i][j]>helpMatrix[maxI][maxJ]){
						maxI = i;
						maxJ = j;
					}
				}else{
					helpMatrix[i][j] = 0;
				}
			}
		}
		printArrayWithStrings(helpMatrix, s1, s2);
		return (maxI==0)?"��� ����� ������":s2.substring(maxI - helpMatrix[maxI][maxJ], maxI);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("������� ��� ������");
		Scanner sc = new Scanner(System.in);
		String s1 = sc.nextLine();
		String s2 = sc.nextLine();
		System.out.println("����� ��������� - " + findSubString(s1, s2));
	}

}
