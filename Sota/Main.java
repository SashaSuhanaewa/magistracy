import javax.swing.*;

public class Main {
    public static MenuPanel menuPanel;

    public static void main(String[] args){
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                menuPanel = new MenuPanel();
            }
        });
    }
}