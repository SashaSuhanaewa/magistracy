import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.LinkedList;

public class Graph {

	private static class Elem {
		public int id;
		public ArrayList<T> elems;

		public Elem(int id) {
			this.id = id;
			elems = new ArrayList<T>();
		}
	}

	static private class T {
		public int id;
		public int length;

		T(int id, int weight) {
			this.id = id;
			length = weight;
		}
	}

	private ArrayList<Elem> elems;
	

	public Graph() {
		elems = new ArrayList<Elem>();
	}

	public Graph(String file) throws Exception {
		elems = new ArrayList<Elem>();

		Scanner sc = new Scanner(new File(file));

		while (sc.hasNext()) {
			int id1 = sc.nextInt();
			int id2 = sc.nextInt();
			int weight = sc.nextInt();

			addVertex(id1);
			addVertex(id2);

			addEdge(id1, id2, weight);

		}
		sc.close();

	}

	public int V() {
		return elems.size();
	}

	public void addVertex(int id) {

		try {
			find(id);
		} catch (Exception e) {
			Elem temp = new Elem(id);
			elems.add(temp);
		}

	}

	public void addEdge(int id1, int id2, int weight) throws Exception {

		if (id1 == id2)
			throw new Exception("Error" );

		find(id1).elems.add(new T(id2, weight));
		find(id2).elems.add(new T(id1, weight));

	}

	private Elem find(int id) throws Exception {
		for (Elem i : elems) {

			if (i.id == id)
				return i;
		}
		throw new Exception("Not found");
	}


	private int findIndex(int id) throws Exception {
		for (int i = 0; i < elems.size(); i++){
			if( elems.get(i).id == id )
				return i;
		}
		throw new Exception("Not found");
	}

	
	public void dfs() throws Exception {
		dfs(elems.get(0).id);
	}

	public void dfs(int id) throws Exception {

		ArrayList<Integer> b = new ArrayList<Integer>();

		dfs(id, b);
	}

	public void dfs(int id, ArrayList<Integer> b) throws Exception {

		System.out.println(id);
		b.add(id);
		Elem temp = find(id);
		for (int i = 0; i < temp.elems.size(); i++) {
			int id1 = temp.elems.get(i).id;
			boolean p = false;
			for (int j : b)
				if (j == id1)
					p = true;
			if (p)
				continue;
			dfs(id1, b);
		}

	}

	public void bfs() throws Exception {
		bfs(elems.get(0).id);
	}

	public void bfs(int id) throws Exception {

		ArrayList<Integer> b = new ArrayList<Integer>();
		LinkedList<Integer> queue = new LinkedList<Integer>();

		queue.addLast(id);
		b.add(id);

		while (!queue.isEmpty()) {

			int j = queue.removeFirst();

			System.out.println(j);

			Elem temp = find(j);

			for (int i = 0; i < temp.elems.size(); i++) {
				int id1 = temp.elems.get(i).id;
				boolean p = false;
				for (int jp : b)
					if (jp == id1)
						p = true;
				if (p)
					continue;
				b.add(id1);
				queue.addFirst(id1);
			}
		}

	}

	public void Krus() throws Exception {

		ArrayList<T> list = new ArrayList<T>();
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
		for (Elem e : elems) {
			map.put(e.id, e.id);
			for (T t : e.elems) {
				list.add(t);
			}
		}
		while (!list.isEmpty()) {
			ArrayList<Integer> a = new ArrayList<Integer>();
			T min = list.get(0);

			for (int i = 0; i < 2; i++) {
				min = list.get(0);
				for (T t : list) {
					if (min.length > t.length) {
						min = t;
					}
				}
				list.remove(min);
				a.add(min.id);
			}

			int id1 = map.get(a.get(0));
			int id2 = map.get(a.get(1));

			if (map.get(id1) == map.get(id2)) {
				continue;
			} else {
				System.out
						.println(a.get(1) + " " + a.get(0) + " " + min.length);
				for (Elem e : elems) {
					if (map.get(e.id) == id2)
						map.put(e.id, id1);
				}
			}

		}
	}

	public int Dekst(int id1, int id2) throws Exception {
		
		int length[] = new int[elems.size()];
		boolean  b[] = new boolean[elems.size()];
	
		
		
		for (int i = 0; i < elems.size(); i++) {
				b[i] = true;
				length[i] = Integer.MAX_VALUE;
		}

		int h = findIndex(id1);
		int j = findIndex(id2);
		
		
		length[h] = 0;
		

		while( b[j]  ){
			
			Elem temp = elems.get(h);
			
			for( int i = 0;  i < temp.elems.size(); i++ ){
				T g = temp.elems.get(i); 
		
				int jj = findIndex(g.id);
				
				length[jj] = Math.min(g.length + length[h], length[jj]); 
				
			}
		
			b[h] = false;
			
			int min = Integer.MAX_VALUE;
			
			for( int i = 0; i < length.length; i++){
				if( min > length[i] && b[i])
					min = i;
			}
			
			h = min;
			
		}
		
		
		return length[j];
		
	}

}
