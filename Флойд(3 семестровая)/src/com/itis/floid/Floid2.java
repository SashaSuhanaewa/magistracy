package com.itis.floid;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumnModel;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class Floid2 extends JFrame {
	private static final int INF = 1000 * 1000 * 1000;
	JPanel panel = new JPanel();
	JPanel panel2 = new JPanel();
	JPanel panel3 = new JPanel();
	JPanel panel4 = new JPanel();
	JFileChooser fileopen = new JFileChooser();
	
	JTextArea display = new JTextArea();
	static JTextField display2 = new JTextField(15);
	static JTextField display3 = new JTextField(15);
	static JTextField display4 = new JTextField(15);
	JLabel label = new JLabel("������� ���������:", SwingConstants.CENTER);
	JLabel label2 = new JLabel("���������� ������:", SwingConstants.CENTER);
	JLabel label3 = new JLabel("���������� ���������� �� ����� �� ������ �������:", SwingConstants.CENTER);
	JLabel finish = new JLabel("�������� �������:", SwingConstants.CENTER);
	JLabel first= new JLabel("��������� �������:", SwingConstants.CENTER);
	JLabel second= new JLabel("�������� �������:", SwingConstants.CENTER);
	JButton vvod = new JButton("������ �������");
	JButton open = new JButton("������� ����");
	JButton okay = new JButton("Ok");
	JButton okay2 = new JButton("Ok");
	String smezh;
	String [] matrix;
	int[][] mass;
	Floid2() {
		
		panel.setLayout(new GridLayout(3, 1, 5, 10));
		panel.add(label);
		panel.add(vvod);
		panel.add(open);
		setContentPane(panel);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setSize(300, 400);
		setLocation((screenSize.width - WIDTH) / 8,
				(screenSize.height - HEIGHT) / 8);
		vvod.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent s) {
				window1();

			}
		});

		open.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent s) {
				int ret = fileopen.showDialog(null, "������� ����");
				if (ret == JFileChooser.APPROVE_OPTION) {
				}
				File file = fileopen.getSelectedFile();
				Scanner in = null;
				int test = 0;
				try {
					in = new Scanner(file);
					smezh="";
					while (in.hasNextLine()) {
						smezh+=in.nextLine()+" ";
						}
					smezh = smezh.replace("\n", " ");
					smezh = smezh.replace("  ", " ");
					matrix = smezh.split(" ");
					int N = (int) Math.sqrt(matrix.length);
					mass = new int[N][N];
					
					if ((matrix.length) != N * N) {
						JOptionPane.showMessageDialog(null,"�� �� ��������� ��������� ����");
					} else {

						int count = 0;
						for (int i = 0; i < N; i++) {
							for (int j = 0; j < N; j++) {
								// ��������� ��� �����

								mass[i][j] = Integer.parseInt(matrix[count]);

								count++;
								// �� ������� ���� g[i][j] = 0, �� ��� ��������,
								// ���
								// ���� �� i � j ���. � ���� ������ ����������
								// �����
								// ����� ��������� ���������� ������
								if (mass[i][j] == 0) {
									mass[i][j] = INF;
								}
							}
						}
						if (test == 0) {
							// ������ ����������� ���������� � �������� ����� �
							// �������
							// ��������� ������
							for (int k = 0; k < N; k++) {
								for (int i = 0; i < N; i++) {
									for (int j = 0; j < N; j++) {
										mass[i][j] = Math.min(mass[i][j],
												mass[i][k] + mass[k][j]);
									}
								}
							}
						}
						finish(mass);// ������� ������� ���������� �����.
					}
					
				}
				
				 catch (FileNotFoundException e) {
					System.out.println("���� �� ������");
					System.out.println(e.getMessage());
				}
				catch (NumberFormatException e) {
					System.out.println("� ����� ���������� �������, ��������� ���������� �����. � ��� ������ ����������� ������ ���� ����� �� ����� ������. \n����� ��������� ����� �������� �����������!");
					System.exit(0);
				}
				in.close();
			}
		});
		setTitle("�������� ������");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(400, 300);
		setVisible(true);
	}

	void window1() {

		setSize(700, 200);
		setResizable(false);
		panel2.add(label2);
		panel2.add(display2);
		display2.setPreferredSize(new Dimension(80, 20));
		panel2.add(label);
		panel2.add(display);
		display.setPreferredSize(new Dimension(100, 100));
		panel2.add(okay);
		okay.setPreferredSize(new Dimension(80, 50));
		okay.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent s) {
				int test = 0;
				try {
					int N = Integer.parseInt(display2.getText());
					mass = new int[N][N];
					smezh = display.getText();
					smezh = smezh.replace("\n", " ");
					smezh = smezh.replace("  ", " ");
					matrix = smezh.split(" ");
					if ((matrix.length) != N * N) {
						JOptionPane
								.showMessageDialog(
										null,
										"�� ����������� ��������� ������� ���������! ���������� ��������� � ������� � ������ ������ ���� ����� ���������� ������");
					} else {

						int count = 0;
						for (int i = 0; i < N; i++) {
							for (int j = 0; j < N; j++) {
								// ��������� ��� �����

								mass[i][j] = Integer.parseInt(matrix[count]);

								count++;
								// �� ������� ���� g[i][j] = 0, �� ��� ��������,
								// ���
								// ���� �� i � j ���. � ���� ������ ����������
								// �����
								// ����� ��������� ���������� ������
								if (mass[i][j] == 0) {
									mass[i][j] = INF;
								}
							}
						}

						if (test == 0) {
							// ������ ����������� ���������� � �������� ����� �
							// �������
							// ��������� ������
							for (int k = 0; k < N; k++) {
								for (int i = 0; i < N; i++) {
									for (int j = 0; j < N; j++) {
										mass[i][j] = Math.min(mass[i][j],
												mass[i][k] + mass[k][j]);
									}
								}
							}

							// ������� ������� ���������� �����.
							finish(mass);
						}
					}
				} catch (Exception e1) {
					JOptionPane
							.showMessageDialog(null,
									"���������� ������ � ������� ������ ��������� ������ ��������� ��������!");
					test = 1;
				}
			}
		});
		setContentPane(panel2);
	}	
	void finish(final int [][]data){
			//panel4.setLayout(new GridLayout(7, 5, 5, 5));
	     
			setSize(350, 200);
			setResizable(false);
	        JTable matrix = createMatrixView(data);
	        matrix.setBackground(panel.getBackground());
	        panel4.add(finish);
	        panel4.add(matrix);
	        panel4.add(label3);
	        
	        panel4.add(first);
			panel4.add(display3);
			display3.setPreferredSize(new Dimension(20, 20));
			panel4.add(second);
			panel4.add(display4);
			display4.setPreferredSize(new Dimension(20, 20));
			panel4.add(okay2);
		    setContentPane(panel4);
	        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        setVisible(true);
	        okay2.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent s) {
					String one = display3.getText();
					String two = display4.getText();
					try{
						JOptionPane.showMessageDialog(null,"���������� ���������� �� ������� " + one + " �� ������� " + two +"  : "+ data[Integer.parseInt(one)-1][Integer.parseInt(two)-1]);
					}
					catch (Exception e){
						JOptionPane.showMessageDialog(null,"Error! ������ ������ ���������� �������� �������� ��� �� ��������� ���� ������� �� ���������� (>N)");
					}
				}
			});
	        
	        
	}
	public JTable createMatrixView(final int[][] data) {
        JTable table = new JTable(new AbstractTableModel() {
            
            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                return data[rowIndex][columnIndex];
            }
            
            @Override
            public int getRowCount() {
                return data.length;
            }
            
            @Override
            public int getColumnCount() {
                return data[0].length;
            }
            
            
        });
 
        table.setShowGrid(false);
        table.setEnabled(false);
        
        TableColumnModel tcm = table.getColumnModel();
        for (int i = 0; i < tcm.getColumnCount(); i++) {
            tcm.getColumn(i).setMaxWidth(20);
        }
 
        return table;
    }

	public static void main(String[] args) throws IOException {
		new Floid2();
	}
}
