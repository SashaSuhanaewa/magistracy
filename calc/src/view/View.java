package view;


import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import calc.*;


public class View extends JFrame {
	

	private static final long serialVersionUID = 1L;

	public View(){
		
		super("Calc");
		
		JPanel panel = new JPanel();
		final JTextField input = new JTextField();

		Font font = new Font("Tahoma",0,25);
		
		input.setHorizontalAlignment(JTextField.RIGHT);
		input.setPreferredSize(new Dimension(250,50));
		input.setFont(font);

		
		input.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			
				try {
					input.setText( ""+ Calc.result(input.getText()));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					input.setText("Error: " + e.getMessage() );
				}

			}
		});	
		
		panel.add(input);
		
		
		getContentPane().add(panel,BorderLayout.PAGE_START);

        final String m[] = new String[17];

        for( int i = 0; i < m.length; i++)
            m[i] = ""+ (i+1);
        m[9] = ".";
        m[10] = "0";

        panel = new JPanel(new GridLayout(4,3));

        for(  int i = 0; i < 11; i++){
            JPanel t = new JPanel();
            final JButton b = new JButton(m[i]);
            t.add(b);
            panel.add(t);
            b.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    input.setText(input.getText() + b.getText());
                }
            });

        }

        JPanel t = new JPanel();
        JButton b = new JButton("=");
        t.add(b);
        panel.add(t);
        b.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {


                try {
                    input.setText( ""+ Calc.result(input.getText()));
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    input.setText("Error: " + e.getMessage() );
                }

            }
        });

        getContentPane().add(panel,BorderLayout.LINE_START);

        m[11] = "(";
        m[12] = ")";
        m[13] = "*";
        m[14] = "/";
        m[15] = "+";
        m[16] = "-";

        panel = new JPanel(new GridLayout(4,3));

        for(  int i = 11; i < 17; i++){
            JPanel tt = new JPanel();
            final JButton bb = new JButton(m[i]);
            tt.add(bb);
            panel.add(tt);
            bb.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    input.setText(input.getText() + bb.getText());
                }
            });

        }


        getContentPane().add(panel,BorderLayout.LINE_END);


    }
	


}
