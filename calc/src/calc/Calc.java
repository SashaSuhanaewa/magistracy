package calc;

public class Calc {
	private static char[] priority = { '+', '-', '*', '/', '%', '^', ' ' };

	static String brackets(String a) {
		return brackets(a, 0, a.length(), true);
	}

	static String brackets(String b, int t, int j, boolean g) {

		String a = new String(b);
		int k = 0;

		for (int i = t; i < j; i++)
			if (a.charAt(i) != ' ')
				k++;
		char c[] = new char[k];
		k = 0;
		for (int i = t; i < j; i++)
			if (a.charAt(i) != ' ')
				c[k++] = a.charAt(i);
		
		int ttt = 0;
		for( int i = 0; i < c.length - 1; i++ ){
			if( c[i] == '(') ttt++;
			if( c[i] == ')') ttt--;
			if( ttt == 0 ) g = false;
		}
		
		for (int i = 0; i < c.length / 2 && g; i++) {
			if ( c.length > 1 && c[i] == '(' && c[c.length - i - 1] == ')')
				c[i] = c[c.length - 1 - i] = ' ';
		}

		return new String(c).toLowerCase();
	}

	static public double result(String a) throws Exception {

		String arg1, arg2;
		char op = 6;
		int ind = 0;
		Exception sc = new Exception("ошибка");
		int s = 0;

		a = brackets(a);

		if (a.length() == 0)
			return 0;
		if (a.equals("pi") || a.equals("пи"))
			return Math.PI;
		if (a.equals("e") || a.equals("е"))
			return Math.E;
		if (a.equals("exit") || a.equals("выход") || a.equals("quit"))
			System.exit(0);
		if(a.equals("infinity"))
			return 1.0/0.0;
		for (int i = 0; i < a.length(); i++) {
			char g = a.charAt(i);
			if (g >= '0' && g <= '9') continue;

			if (g == '(') s++;
			if (g == ')') {
				s--;
				if (s < 0)
					throw sc;
			}

			for (int k = 0; k < priority.length; k++)
				if (g == priority[k] && k < op && s == 0) {
					ind = i;
					op = (char) k;
				}

		}

		if (s != 0) throw sc;

		op = priority[op];

		arg1 = brackets(a, 0, ind, true);
		arg2 = brackets(a, ind+1, a.length(), true);

		switch (op) {
		case '-':
			return result(arg1) - result(arg2);
		case '+':
			return result(arg1) + result(arg2);
		case '*':
			return result(arg1) * result(arg2);
		case '/':
			return result(arg1) / result(arg2);
		case '%':
			return result(arg1) % result(arg2);
		case '^':
			return (float) Math.pow(result(arg1), result(arg2));
		case ' ':

			a = brackets(a);

			boolean par = true;
			for (int i = 0; par && i < 6 && i < a.length(); i++) {
				if (a.charAt(i) == '(') {
					par = false;
					ind = i;
				}
			}

			arg1 = brackets(a, 0, ind, true);
			arg2 = brackets(a, ind, a.length(), true);
			
			if (arg1.equals("cos")) return Math.cos(result(arg2));
			if (arg1.equals("sin")) return Math.sin(result(arg2));
			if (arg1.equals("tan")) return Math.tan(result(arg2));
			if (arg1.equals("log")) return Math.log(result(arg2));
			if (arg1.equals("log10")) return Math.log10(result(arg2));

			return Double.parseDouble(a);

		}

		return 0;

	}

}
