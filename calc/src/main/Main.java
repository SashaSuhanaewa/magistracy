package main;


import view.View;

public class Main {
    public static void main(String[] args) {
    View frame = new View();
    frame.pack();
    frame.setVisible(true);
    frame.setLocation(400, 400);
    frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
}
}
