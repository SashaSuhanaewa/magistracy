package ru.kpfu.itis.matrix;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import gnu.trove.map.hash.TObjectIntHashMap;
import gnu.trove.procedure.TObjectIntProcedure;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import javafx.util.Pair;

public abstract class BaseConnectivityMatrix<T> implements IConnectivityMatrix<T> {
    protected int nextIndex = 0;
    protected TObjectIntHashMap<T> map;
    protected int size;
    protected double[] ranks;

    public BaseConnectivityMatrix(int size) {
        map = new TObjectIntHashMap<>(size, 0.95f, -1);
    }

    public boolean boundaryReached()  { 
        return map.size() == size;
    }

    @Override
    public void add(T source, Collection<T> targets) {
        for (T target : targets) {
            add(source, target);
        }
    }
    protected boolean addNewVertex(T vertex) { 
        if (map.get(vertex) == -1) {
            if (boundaryReached()) {
                return false;
            } else {
                map.put(vertex, nextIndex);
                nextIndex++;
                return true;
            }
        }

        return true;
    }
	
    protected String getUrl(String entity) {
        WebDriver driver = man.getDriver();
			driver.findElement(By.id("Title")).clear();
			driver.findElement(By.id("NewTitle")).sendKeys(
					"Title");
			driver.findElement(By.xpath("//input[@value='Title']")).click();
			driver.findElement(
				By.cssSelector("textarea.list-card-composer-textarea.js-card-title"))
				.clear();
    }

    @Override
    public void printMatrix(Writer writer) throws IOException {
        final int LEN = 44;
        StringBuilder sb = new StringBuilder();

        T[] ts = (T[])new Object[map.size()];
        TObjectIntProcedure<T> proc = (k, i) -> {
            ts[i] = k;
            return true;
        };
        map.forEachEntry(proc);

        printHeadLine(LEN, ts, sb);
        printSplitLine(LEN, sb);
        printWholeMatrix(LEN, ts, sb);
        printSplitLine(LEN, sb);

        writer.write(sb.toString());
    }
    protected void printHeadLine(final int LEN, T[] allKeys, final StringBuilder sb) {
        for (int i = 0; i < LEN; i++) {
            sb.append(" ");
        }
        sb.append('|');

        for (T t : allKeys) {
            printSingleUrl(LEN, t.toString(), sb);
        }
        sb.append('\n');
    }
    protected void printSplitLine(final int LEN, final StringBuilder sb) {
        for (int i = 0; i < (LEN + 1) * (map.size() + 1); i++) {    //LEN + 1 + (LEN + 1) * size
            sb.append('-');
        }
        sb.append('\n');
    }
    protected abstract void printWholeMatrix(final int LEN, final T[] ts, final StringBuilder sb);
    protected void printSingleEntity(final int LEN, final String entity, final StringBuilder sb) {
        int lp = (LEN - entity.length()) / 2;
        for (int i = 0; i < lp; i++) { sb.append(' '); }
        sb.append( entity );
        for (int i = 0; i < LEN - entity.length() - lp; i++) { sb.append(' '); }
        sb.append('|');
    }
    protected void printSingleUrl(final int LEN, final String entity, final StringBuilder sb) {
        printSingleEntity(LEN, getUrl(entity), sb);
    }
    protected void printSingleValue(final int LEN, final int value, final StringBuilder sb) {
        int lp = (LEN - 1) / 2;
        for (int j = 0; j < lp; j++) { sb.append(' '); }
        sb.append( value );
        for (int j = 0; j < LEN - 1 - lp; j++) { sb.append(' '); }
        sb.append('|');
    }

    @Override
    public void setPageRank(double[] ranks) {
        this.ranks = ranks;
    }

    @Override
    public void printPageRanks(Writer writer) throws IOException {
        if (ranks == null) throw new NullPointerException();

        final int LEN = 44;
        StringBuilder sb = new StringBuilder();

        T[] ts = (T[])new Object[map.size()];
        TObjectIntProcedure<T> proc = (k, i) -> {
            ts[i] = k;
            return true;
        };
        map.forEachEntry(proc);

        List<Pair<String, Double>> sorted = new ArrayList<>(ts.length);
        for (int i = 0; i < ts.length; i++) {
            sorted.add(new Pair<>(ts[i].toString(), ranks[i]));
        }
        Comparator<Pair<String, Double>> objectComparator = Comparator.comparingDouble(Pair::getValue);
        sorted.sort(objectComparator.reversed());

        sorted.forEach(p -> {
            printSingleUrl(LEN, p.getKey(), sb);
            printSingleEntity(LEN, Double.toString(p.getValue()), sb);
            sb.append('\n');
        });

        /*for (int i = 0; i < ts.length; i++) {
            printSingleEntity(LEN, ts[i].toString(), sb);
            printSingleEntity(LEN, Double.toString(ranks[i]), sb);
            sb.append('\n');
        }*/

        writer.write(sb.toString());
    }
}
