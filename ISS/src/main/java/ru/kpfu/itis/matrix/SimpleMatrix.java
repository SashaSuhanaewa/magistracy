package ru.kpfu.itis.matrix;

import java.util.BitSet;

public class SimpleMatrix<T> extends BaseConnectivityMatrix<T> {
    private BitSet connectivityMatrix;

    public SimpleMatrix(int size) {
        super(size);
        this.size = size;
        connectivityMatrix = new BitSet(size*size);
    }

    @Override
    public void add(T source, T target) {
        if (addNewVertex(source) && addNewVertex(target)) {
            connectivityMatrix.set( map.get(source) * size + map.get(target) );
        }
    }

    @Override
    public void finishBuilding() {
        //nothing to do
    }

    @Override
    public double[][] toRankMatrix() {
        double[][] res = new double[size][size];

        for (int i = 0; i < size; i++) {
            int deg = connectivityMatrix.get(i * size, (i+1) * size).cardinality();  //connectivityMatrix.get(i*size + 0, i * size + size)

            if (deg != 0) {
                for (int j = 0; j < size; j++) {
                    res[i][j] = connectivityMatrix.get(i*size + j) ? (1.0 / deg) : 0.0;
                }
            }
        }

        return res;
    }

    @Override
    public SparseDoubleMatrix toSparseRankMatrix() {
        int[] rowsPointers = new int[size];
        int[] columns = new int[ connectivityMatrix.cardinality() ];
        double[] values = new double[size];

        //set rows and columns
        int ind = -1;
        for (int i = 0; i < columns.length; i++) {
            ind = connectivityMatrix.nextSetBit(ind+1);
            int row = ind / size;
            int col = ind % size;

            rowsPointers[row]++;
            columns[i] = col;
        }

        //set values
        for (int i = 0; i < rowsPointers.length; i++) {
            values[i] = rowsPointers[i] == 0 ? 0.0 : (1.0 / rowsPointers[i]);
        }

        return new SparseDoubleMatrix(rowsPointers, columns, values);
    }


    @Override
    protected void printWholeMatrix(final int LEN, final T[] ts, final StringBuilder sb) {
        for (int i = 0; i < map.size(); i++) {
            printMatrixLine(LEN, ts[i].toString(), i, sb);
        }
    }
    private void printMatrixLine(final int LEN, final String key, final int index, final StringBuilder sb) {
        printSingleEntity(LEN, key, sb);
        int row = index * size;
        for (int i = 0; i < map.size(); i++) {
            printSingleValue(LEN, connectivityMatrix.get( row + i ) ? 1 : 0, sb);
        }
        sb.append('\n');
    }
}
