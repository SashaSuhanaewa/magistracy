package ru.kpfu.itis.matrix;

import gnu.trove.list.TIntList;
import gnu.trove.list.array.TIntArrayList;

public class SparseMatrixParallel<T> extends BaseMultiThreadMatrix<T> {
    private int[] columns;
    private int[] rowPointers;

    public SparseMatrixParallel(int size) {
        super(size);
        this.columns = new int[size*size];
    }

    @Override
    public void add(T source, T target) {
        if (addNewVertex(source) && addNewVertex(target)) {
            columns[map.get(source) * size + map.get(target)] = 1;
        }
    }

    @Override
    public void finishBuilding() {                        //Can not be effectively parallelized
        rowPointers = new int[size];
        TIntList newCols = new TIntArrayList(2 * size ,-1);

        for (int i = 0; i < columns.length; i++) {
            if (columns[i] == 1)  {
                rowPointers[i / size]++;
                newCols.add(i % size);
            }
        }
        columns = newCols.toArray();
    }

    @Override
    public double[][] toRankMatrix() {                     //Do not need parallelization
        double[][] res = new double[size][size];

        int colIndex = 0;
        for (int i = 0; i < size; i++) {
            int l = rowPointers[i];

            if (l != 0) {
                for (int j = 0; j < l; j++) {
                    res[i][columns[colIndex]] = 1.0 / l;
                    colIndex++;
                }
            }
        }

        return res;
    }

    @Override
    public SparseDoubleMatrix toSparseRankMatrix() {       //Do not need parallelization
        double[] values = new double[rowPointers.length];
        for (int i = 0; i < size; i++) {
            values[i] = rowPointers[i] == 0 ? 0.0 : (1.0 / rowPointers[i]);
        }

        return new SparseDoubleMatrix(rowPointers, columns, values);
    }

    @Override
    protected void printWholeMatrix(int LEN, T[] ts, StringBuilder sb) {      //Can not be parallelized
        int nextCol = 0;

        for (int i = 0; i < rowPointers.length; i++) {   // == map.size()
            printSingleUrl(LEN, ts[i].toString(), sb);

            int prev = 0;
            if (rowPointers[i] == 0) {
                printZeros(LEN, sb, map.size());
            } else {
                for (int j = 0; j < rowPointers[i]; j++) {
                    int lp = columns[nextCol] - prev;
                    printZeros(LEN, sb, lp);
                    nextCol++;
                    printSingleValue(LEN, 1, sb);
                    prev = columns[nextCol-1] + 1;
                }
                printZeros(LEN, sb, map.size() - prev);
            }

            sb.append('\n');
        }
    }
    private void printZeros(final int LEN, final StringBuilder sb, final int n) {
        for (int i = 0; i < n; i++) {
            printSingleValue(LEN, 0, sb);
        }
    }
}
