package ru.kpfu.itis.matrix;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import javafx.util.Pair;

public abstract class BaseMultiThreadMatrix<T> implements IConnectivityMatrix<T> {
    protected AtomicInteger nextIndex; 
    protected ConcurrentHashMap<T, Integer> map; 
    protected final int size;
    protected double[] ranks;

    public BaseMultiThreadMatrix(int size) {
        this.size = size;
        this.map = new ConcurrentHashMap<>(size, 0.95f);
        this.nextIndex = new AtomicInteger(0);
    }

    public boolean boundaryReached()  {
        return map.size() == size;
    }

    protected boolean addNewVertex(T vertex) {
        if ( ! map.containsKey(vertex)) {
            if (boundaryReached()) {
                return false;
            } else {
                map.put(vertex, nextIndex.getAndIncrement());
                return true;
            }
        }

        return true;
    }

    @Override
    public void add(T source, Collection<T> targets) {
        for (T target : targets) {
            add(source, target);
        }
    }

    protected String getUrl(String entity) {
        return "https://www.wikidata.org/wiki/" + entity;
    }

    @Override
    public void printMatrix(Writer writer) throws IOException {
        final int LEN = 36;
        StringBuilder sb = new StringBuilder();

        T[] ts = (T[])new Object[map.size()];
        map.forEach( (k, i) -> ts[i] = k);

        printHeadLine(LEN, ts, sb);
        printSplitLine(LEN, sb);
        printWholeMatrix(LEN, ts, sb);
        printSplitLine(LEN, sb);

        writer.write(sb.toString());
    }
    protected void printHeadLine(final int LEN, T[] allKeys, final StringBuilder sb) {
        for (int i = 0; i < LEN; i++) {
            sb.append(" ");
        }
        sb.append('|');

        for (T t : allKeys) {
            printSingleUrl(LEN, t.toString(), sb);
        }
        sb.append('\n');
    }
    protected void printSplitLine(final int LEN, final StringBuilder sb) {
        for (int i = 0; i < (LEN + 1) * (map.size() + 1); i++) {    //LEN + 1 + (LEN + 1) * size
            sb.append('-');
        }
        sb.append('\n');
    }
    protected abstract void printWholeMatrix(final int LEN, final T[] ts, final StringBuilder sb);
    protected void printSingleEntity(final int LEN, final String entity, final StringBuilder sb) {
        int lp = (LEN - entity.length()) / 2;
        for (int i = 0; i < lp; i++) { sb.append(' '); }
        sb.append( entity );
        for (int i = 0; i < LEN - entity.length() - lp; i++) { sb.append(' '); }
        sb.append('|');
    }
    protected void printSingleUrl(final int LEN, final String entity, final StringBuilder sb) {
        printSingleEntity(LEN, getUrl(entity), sb);
    }
    protected void printSingleValue(final int LEN, final int value, final StringBuilder sb) {
        int lp = (LEN - 1) / 2;
        for (int j = 0; j < lp; j++) { sb.append(' '); }
        sb.append( value );
        for (int j = 0; j < LEN - 1 - lp; j++) { sb.append(' '); }
        sb.append('|');
    }

    @Override
    public void setPageRank(double[] ranks) {
        this.ranks = ranks;
    }

    @Override
    public void printPageRanks(Writer writer) throws IOException { //ранги выводятся. Тут нужно сортировать
        if (ranks == null) throw new NullPointerException(); //слуужебные операции

        final int LEN = 44;
        StringBuilder sb = new StringBuilder();

        T[] ts = (T[])new Object[map.size()];
        map.forEach( (k, i) -> ts[i] = k);

        List<Pair<String, Double>> sorted = new ArrayList<>(ts.length); // до 128 строки происходит сортировка. 
        for (int i = 0; i < ts.length; i++) { //нужно сортировать пару значений - ссылка и пейдж ранг. сортируем по пейдж рангу
            sorted.add(new Pair<>(ts[i].toString(), ranks[i])); //делаем список пар из данных. из строки и числа
        }
        Comparator<Pair<String, Double>> objectComparator = Comparator.comparingDouble(Pair::getValue); //задаем логику сортировку Сортируем по числу по убыванию
        sorted.sort(objectComparator.reversed()); //сортировка

        sorted.forEach(p -> { //вывод ранга
            printSingleUrl(LEN, p.getKey(), sb);
            printSingleEntity(LEN, Double.toString(p.getValue()), sb);
            sb.append('\n');
        });

        /*for (int i = 0; i < ts.length; i++) {
            printSingleEntity(LEN, ts[i].toString(), sb);
            printSingleEntity(LEN, Double.toString(ranks[i]), sb);
            sb.append('\n');
        }*/

        writer.write(sb.toString());
    }
}
