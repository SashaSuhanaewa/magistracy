package ru.kpfu.itis.pagerank;

import ru.kpfu.itis.matrix.SparseDoubleMatrix;

public interface IPageRankMethod {
    double[] calculatePageRank(double[][] matrix, double eps);

    double [] calculate (SparseMatrix sparseMatrix);
	
}
