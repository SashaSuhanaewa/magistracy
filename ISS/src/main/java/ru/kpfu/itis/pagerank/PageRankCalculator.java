package ru.kpfu.itis.pagerank;

import ru.kpfu.itis.matrix.IConnectivityMatrix;

public class PageRankCalculator {
    private IPageRankMethod method;

    public PageRankCalculator(IPageRankMethod method) {
        this.method = method;
    }

    public void assignPageRanks(IConnectivityMatrix<?> matrix, double eps) {
        matrix.setPageRank(
                method.calculatePageRank(matrix.toSparseRankMatrix(), eps)
        );
    }

    public void setPageRankMethod(IPageRankMethod method) {
        this.method = method;
    }
}
