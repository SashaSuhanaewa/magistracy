package ru.kpfu.itis.pagerank;

import java.util.Arrays;
import java.util.stream.IntStream;
import ru.kpfu.itis.matrix.SparseDoubleMatrix;

public class PowerModePageRankMethod extends BasePageRankMethod {
    @Override
    protected double[] getFirstApproximation(double[][] matrix) {
        double[] res = new double[matrix.length];
        double value = Math.sqrt( 1.0 / res.length );
        Arrays.fill(res, value);
        return res;
    }

    @Override
    protected double[] getFirstApproximation(SparseDoubleMatrix matrix) {
        double[] res = new double[matrix.size()];
        double value = Math.sqrt( 1.0 / res.length );
        Arrays.fill(res, value);
        return res;
    }

    @Override
    protected double[] produceStep(double[] vector, double[][] matrix) {
        if (matrix.length == 0) return new double[0];

        /*double[] res = new double[matrix.length];
        for (int j = 0; j < matrix[0].length; j++) {
            for (int i = 0; i < vector.length; i++) {
                res[j] += matrix[i][j] * vector[i];
            }
        }*/
        return Arrays.stream(matrix).mapToDouble(row ->
            IntStream.range(0, row.length).mapToDouble(col -> row[col]*vector[col]).sum()
        ).toArray();

        //return res;
    }

    @Override
    protected double[] produceStep(double[] vector, SparseDoubleMatrix matrix) {
        return matrix.multiplyByVector(vector);
    }
}
