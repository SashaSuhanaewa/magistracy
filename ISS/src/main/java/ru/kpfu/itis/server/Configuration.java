package ru.kpfu.itis.server;

import org.kohsuke.args4j.Option;

public class Configuration {
    @Option(name = "-l", usage = "capacity of connectivity matrix", required = true)
    private int size;

    @Option(name = "-e", usage = "initial wikidata entity to start build matrix from", required = false)
    private int initialEntity = -1;

    @Option(name = "-o", usage = "paths to files of output information", required = true)
    private String outpath;

    public int getSize() {
        return size;
    }

    public int getInitialEntity() {
        return initialEntity;
    }

    public String getOutpath() {
        return outpath;
    }
}
