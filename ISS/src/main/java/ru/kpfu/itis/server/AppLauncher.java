package ru.kpfu.itis.server;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import ru.kpfu.itis.matrix.IConnectivityMatrix;
import ru.kpfu.itis.pagerank.PageRankCalculator;
import ru.kpfu.itis.pagerank.PowerModePageRankMethod;

public class AppLauncher {
    public static void main(String[] args) {
        Configuration config = new Configuration();
        CmdLineParser parser = new CmdLineParser(config);

        try {
            parser.parseArgument(args);
        } catch (CmdLineException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }

        final String OUTDIR = config.getOutpath();
        System.out.println("Matrix building started!");

        long t = System.currentTimeMillis(); 
        IConnectivityMatrix<String> matrix = new MatrixBuilder().buildMatrix( config.getSize(), config.getInitialEntity() );
        t = System.currentTimeMillis() - t;
        System.out.println("Matrix is ready in " + t + " msecs! \nStart parallel matrix building");

        long t1 = System.currentTimeMillis(); 
        IConnectivityMatrix<String> matrix1 = new MatrixBuilder().buildMatrix( config.getSize(), config.getInitialEntity() );
        t1 = System.currentTimeMillis() - t1;
        System.out.println("Paralleled matrix is ready in " + t1 + " msecs! ");

        System.out.println("Speed coefficiency is:  " + (t / (double)t1)); 
        System.out.println("Start printing matrix");

        new File(OUTDIR).mkdirs();
        String outName = OUTDIR + "matrix (initial - Q" + (config.getInitialEntity() < 0 ? 1 : config.getInitialEntity()) + ").txt";
        File f = new File(outName);
        try (Writer w = new FileWriter(f.getAbsolutePath())) {
            w.write("Matrix:\n");
            matrix.printMatrix(w);
        } catch (IOException e) {
            System.err.println(" Problems with printing matrix! Application is being interrupted! ");
            e.printStackTrace();
            System.exit(1);
        }


        System.out.println("Matrix was printed! \n\nStart calculate ranks!");

        t = System.currentTimeMillis();
        new PageRankCalculator( new PowerModePageRankMethod() ).assignPageRanks(matrix, 1e-3);
        t = System.currentTimeMillis() - t;
        System.out.println("Ranks were caluclated in:  " + t + " msecs!");

        outName = OUTDIR + "ranks (initial - Q" + (config.getInitialEntity() < 0 ? 1 : config.getInitialEntity()) + ").txt";
        try (Writer w = new FileWriter(outName)) {
            w.write("PageRank:\n");
            matrix.printPageRanks(w);
        } catch (IOException e) {
            System.err.println(" Problems with printing ranks! Application is being interrupted! ");
            e.printStackTrace();
            System.exit(1);
        }

        System.out.println("Finished!");
        try {   System.in.read();  }      //enter waiting
        catch (IOException e) {   e.printStackTrace();    }
    }
}
