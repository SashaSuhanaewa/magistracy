package ru.kpfu.itis.server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Set;
import ru.kpfu.itis.matrix.IConnectivityMatrix;

public abstract class AbstractMatrixBuilder {
    protected static final String QUERY =
            "SELECT v.val " +
            "FROM value_snaks v INNER JOIN ieids i ON v.val = i.eid " +
            "WHERE v.entity_id='Q1'; ";

    protected Connection getConnection() {
        try {
            return DriverManager.getConnection(
                    "jdbc:postgresql://127.0.0.1:5432/wikidata",
                    "postgres",
                    "root"
            );
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    protected void makeQuery(Connection conn, IConnectivityMatrix<String> matrix, Set<String> set, String entity) {
        try {
            PreparedStatement ps = conn.prepareStatement( QUERY.replace("'Q1'", "'" + entity + "'") );
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                String target = rs.getString(1);
                set.add(target);
                matrix.add(entity, target);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public abstract IConnectivityMatrix<String> buildMatrix(int size, int initialEntity);
}
