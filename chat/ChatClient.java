package sockets.chat;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class ChatClient extends JFrame{
	private JTextField usernameField;
	private JComboBox<String> hosts;
	private JComboBox<Integer> ports;
	
	public ChatClient() {
		super("Please, log in!"); 
		setLocation(150, 150);
		setSize(600, 500);
		setLayout(new BorderLayout());
			JButton button = new JButton("Connect");			
			
			JPanel autorizationPanel = new JPanel(new GridLayout(3, 1));
				usernameField = new JTextField();	
				autorizationPanel.add(initSector(new JLabel(" Enter your username: "), usernameField));
				
				String[] enabledHosts = {"localhost"};
				hosts = new JComboBox<>(enabledHosts);
				autorizationPanel.add(initSector(new JLabel(" Choose the connecting host: "), hosts));
				
				Integer[] enabledPorts = {7000};	
				ports = new JComboBox<>(enabledPorts);
				autorizationPanel.add(initSector(new JLabel(" Choose the connecting port: "), ports));
			
		
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				final String u;
				if ((u = usernameField.getText()).length() > 0) {
					final String host = (String) hosts.getSelectedItem();
					final Integer port = (Integer) ports.getSelectedItem();
					setVisible(false);
					dispose();
					new Thread(new Runnable() {
						@Override
						public void run() {
							new ChatClientWindow(u, host, port).run();
						}
					}).start();
				}
			}
		});
		add(button, BorderLayout.SOUTH);
		add(autorizationPanel, BorderLayout.CENTER);
		
		pack();
		setVisible(true);
	}
	
	public JPanel initSector(JLabel label, JComponent component) {
		JPanel p = new JPanel(new GridLayout(2, 1));
		p.setBorder(BorderFactory.createEtchedBorder());
		p.add(setFont(label));
		p.add(setFont(component));
		return p;
	}
	
	private JComponent setFont(JComponent src) {
		src.setFont(new Font(src.getFont().getFontName(), Font.BOLD, 14));
		return src;
	}
	
	public static void main(String[] args) {
		new ChatClient();
	}
}