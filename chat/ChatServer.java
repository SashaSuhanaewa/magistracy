package sockets.chat;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class ChatServer {
	List<ChatClientThread> listOfClients = new ArrayList<>();
//	public static final String PATH = "history.txt";
//	public static File historyFile;
//	private PrintWriter historyWriter;
	
//	{
//		try {
//			historyFile = new File(PATH);
//			historyFile.createNewFile();
//			historyWriter = new PrintWriter(new FileWriter(historyFile), true);
//		} catch (IOException e1) {
//			e1.printStackTrace();
//		}
//	}
	
	private class ChatClientThread extends Thread {
		private Socket socket;
		
		public ChatClientThread(Socket k) {
			socket = k;
		}
		
		public void sendMessage(String msg) {
			try {
				new PrintWriter(this.socket.getOutputStream(), true).println(msg);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		@Override
		public void run() {
			try {
				//PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
				BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				String s;
				while (!socket.isClosed() && (s = in.readLine()) != null) {
					if (s.length() > 0) {
						//historyWriter.println(s);
						for (ChatClientThread c : listOfClients) {
							c.sendMessage(s);
						}
					}
				}
			} catch (IOException e) {
				System.out.println("Клиент вышел");
			}
		}
	}
	
	public void run() {
		try (ServerSocket ss = new ServerSocket(7000)) {
			while (true) {
				Socket k = ss.accept();
				ChatClientThread c = new ChatClientThread(k);
				listOfClients.add(c);
				c.start();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		new ChatServer().run();
	}
}