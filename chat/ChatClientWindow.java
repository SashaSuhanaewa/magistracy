package sockets.chat;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JTextArea;

@SuppressWarnings("serial")
public class ChatClientWindow extends JFrame{
	private String username;
	private String host;
	private Integer port;
	
	private BufferedReader in;
	private PrintWriter out;
	
	private JTextArea chatText;
	private JTextArea clientText;
	private JButton button;
	//private JScrollPane scroll;
	
	public ChatClientWindow(String username, String host, Integer port) {
		this.username = username;
		this.host = host;
		this.port = port;
	}
	
	private void init(final String username) {
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocation(150, 150);
		setSize(600, 500);
		setLayout(null);
		
		//chatText = new JTextArea(getHistory());
		chatText = new JTextArea();
		chatText.setLineWrap(true);
		chatText.setAutoscrolls(true);
		setFont(chatText).setBorder(BorderFactory.createEtchedBorder());
		chatText.setSize(getComponentDimension(0.98, 0.75));
		chatText.setEditable(false);
		chatText.setLocation(0, 0);
		this.add(chatText);
		
		clientText = new JTextArea();
		clientText.setLineWrap(true);
		setFont(clientText).setBorder(BorderFactory.createEtchedBorder());
		clientText.setSize(getComponentDimension(0.75, 0.20));
		clientText.setLocation(0, (int) Math.round(chatText.getSize().getHeight()));
		this.add(clientText);
		
		button = new JButton("Send");
		button.setSize(getComponentDimension(0.22, 0.20));
		button.setLocation((int) Math.round(clientText.getSize().getWidth()), 
				(int) Math.round(chatText.getSize().getHeight()));
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String text;
				if ((text = clientText.getText()).length() > 0) {
					out.println(username + ": " + text);
					clientText.setText("");
				}
			}
		});
		add(button);
		
//		scroll = new JScrollPane();
//		scroll.setSize(getComponentDimension(0.22, 0.74));
//		scroll.setLocation((int) Math.round(chatText.getSize().getWidth()), 0);
//		add(scroll);
		
		setTitle(username + " Chat Window ");
		setVisible(true);
	}
	
	private Dimension getComponentDimension(double widthCoef, double heightCoef) {
		Dimension d = this.getSize();
		int w = (int) Math.round(d.getWidth() * widthCoef);
		int h = (int) Math.round(d.getHeight() * heightCoef);
		return new Dimension(w, h);
	}
	
	public void run() {
		try (Socket s = new Socket(host, port)) {
			in = new BufferedReader(new InputStreamReader(s.getInputStream()));
			out = new PrintWriter(s.getOutputStream(), true);
			new Thread(new Runnable() {
				@Override
				public void run() {
					init(username);
				}
			}).start();	
			String msg = null;
			while ((msg = in.readLine()) != null) {
				chatText.setText(chatText.getText() + msg + "\n");
			}
			in.close();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private JComponent setFont(JComponent src) {
		src.setFont(new Font(src.getFont().getFontName(), src.getFont().getStyle(), 16));
		return src;
	}
	
//	private String getHistory() {
//		String res = "";
//		try {
//			BufferedReader historyReader = new BufferedReader(new FileReader(new File(ChatServer.PATH)));
//			while (historyReader.ready()) {
//				res += historyReader.readLine() + "\n";
//			}
//			historyReader.close();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		return res;
//	}
}