//�������.
public class MyQueue {
	static class ElemInt{
		int value;
		ElemInt next;
	}
	static class ElemString{
		String value;
		ElemString next;
	}
	
	private ElemInt beginInt;
	private ElemInt endInt;
	private ElemString beginString;
	private ElemString endString;
	
	MyQueue(){
		beginInt = endInt = null;
		beginString = endString = null;
	}
	
	public void addInt( int a ){
		ElemInt temp = new ElemInt();
		temp.value = a;
		temp.next = null;
		
		if( emptyInt() || endInt == null ){
			beginInt = endInt = temp;
		}else{
			endInt.next = temp;
			endInt = temp;
		}
	}
	public void addString( String a ){
		ElemString temp = new ElemString();
		temp.value = new String(a);
		temp.next = null;
		
		if( emptyString() || endString == null ){
			beginString = endString = temp;
		}else{
			endString.next = temp;
			endString = temp;
		}
	}
	public int getInt(){
		int temp = beginInt.value;
		beginInt = beginInt.next;
		return temp;
	}
	
	public String getString(){
		String temp = beginString.value;
		beginString = beginString.next;
		return temp;
	}
	
	public boolean emptyInt(){
		return beginInt == null;
	}
	public boolean emptyString(){
		return beginString == null;
	}
	public static void main(String[] args) {
		MyQueue queue = new MyQueue();
		queue.addString("4");
		queue.addString("1");
		queue.addString("6");
		queue.addString("3");
		queue.addString("5");
		
		while(!queue.emptyString())
			System.out.println(queue.getString());
		
		queue.addString("ate");
		queue.addString("3");
		queue.addString("art");
		queue.addString("7");
		queue.addString("w6wisf");
		
		while(!queue.emptyString())
			System.out.println(queue.getString());
	}
}