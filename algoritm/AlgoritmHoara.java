import java.util.Scanner;
import java.util.Stack;

//���������� ����� ����� ����.

public class AlgoritmHoara {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int m = sc.nextInt();
        int[] b = new int[m];
        for (int i = 0; i < m; i++) {
            b[i] = sc.nextInt();
        }
        Stack<Integer> stack = new Stack();
        stack.push(0);
        stack.push(m - 1);
        while (!stack.empty()) {
            int l = stack.pop();
            int r = stack.pop();
            int j = r;
            int i = l;
            int x = b[(r + l) / 2];
            while (i <= j) {
                while (b[i] < x)
                    i++;
                while (b[j] > x)
                    j--;
                if (i <= j) {
                    int k = b[i];
                    b[i] = b[j];
                    b[j] = k;
                    i++;
                    j--;
                }
            }
            if (i < r) {
                stack.push(i);
                stack.push(r);
            }
            if (j > l) {
                stack.push(l);
                stack.push(j);
            }
        }
        for (int i = 0; i < m; i++) {
            System.out.print(b[i] + " ");
        }
    }
}