import java.util.Stack;
import java.util.Random;

//�������� ��� ������� ����� ����.

public class FonNeiman {

	static void h(Stack<Integer> s, int b, int e) {

		if (b < e) {
			s.push(e);
			s.push(b);
			int mid = (e + b) / 2;

			h(s, b, mid);
			h(s, mid+1, e);
		}
	}

	public static void Sort(int a[], int b, int e) {
		Stack<Integer> s = new Stack<Integer>();

		h(s, b, e);

		while (!s.empty()) {
			b = s.pop();
			e = s.pop();
			int mid = (b + e) / 2;
			int pos1 = b;
			int pos2 = mid + 1;
			int pos3 = 0;
			int temp[] = new int[e - b + 1];
			while (pos1 <= mid && pos2 <= e) {
				if (a[pos1] < a[pos2])
					temp[pos3++] = a[pos1++];
				else
					temp[pos3++] = a[pos2++];
			}
			while (pos2 <= e)
				temp[pos3++] = a[pos2++];
			while (pos1 <= mid)
				temp[pos3++] = a[pos1++];

			for (pos3 = 0; pos3 < e - b + 1; pos3++)
				a[b + pos3] = temp[pos3];
		}
	}

	public static void main(String[] args) {
		Random random = new Random();
		int a[] = new int[17];

		for (int i = 0; i < a.length; i++) {
			a[i] = random.nextInt(100);
		}

		for (int i = 0; i < a.length; i++) {
			System.out.print(a[i] + " ");
		}

		Sort(a, 0, a.length - 1);

		System.out.println();
		for (int i = 0; i < a.length; i++) {
			System.out.print(a[i] + " ");
		}
	}
}