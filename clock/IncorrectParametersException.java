public class IncorrectParametersException extends java.lang.Exception {
    
    public IncorrectParametersException() {
        this("IncorrectParameters");
    }
    
    public IncorrectParametersException(String msg) {
        super(msg);
    }
}
