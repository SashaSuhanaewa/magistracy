package controller;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Tree {

	Node root;
	ArrayList<Node> list;
	
	
	Tree( String s ) throws Exception{
		list = new ArrayList<Node>();
		
		
		root = new Node();
		root.parent = null;
		root.value = ' ';
		
		list.add(root);
		
		@SuppressWarnings("resource")
		Scanner sc = new Scanner( new File(s) );
	
		while( sc.hasNextLine() ){
			
			s = sc.nextLine();
			String m[] = s.split(" ");
	
			Node temp = root;
			
			for( int i = m.length - 1; i > 0; i-- ){
				
				if( m[i].equals(".") ){
					
					if( temp.left == null ){
						Node n = new Node();
						n.b = false;
						n.parent = temp;
						temp.left = n;
					}
					temp = temp.left;
					
				}else if( m[i].equals("-") ){
					
					if( temp.right == null ){
						Node n = new Node();
						n.b = true;
						n.parent = temp;
						temp.right = n;
					}
					temp = temp.right;
					
				}else{
					throw new Exception("error in file"); 
				}

			}
			
			if( m[0].length() == 1 ){
				temp.value = m[0].charAt(0);
				list.add(temp);
			}
			
		}
		
		sc.close();
		
	}

	public Node getRoot(){
		return root;
	}
	
	public void dfs(String string) throws Exception  {
		
		PrintWriter out = new PrintWriter(string);
		
		dfs(getRoot(),out, new String());
		
		out.close();
		
		
	}

	private void dfs(Node root2,PrintWriter out, String s) throws Exception {
		
		
		if(root2.left != null){
			out.println(s + "	" + "����� ���������");
			dfs(root2.left,out, "|	" + s);
		}
		
		out.println(s + root2.value);
		
		if(root2.right != null){
			out.println(s + "	" + "������ ���������");
			dfs(root2.right,out, "|	" + s);
		}		
		
	}
	
	
}
