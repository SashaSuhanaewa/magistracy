package controller;

public class Controller {

	static Tree eng;
	static Tree rus;
	
	public static void load() throws Exception{
		
		eng = new Tree("morse.txt");
		rus = new Tree("morseRus.txt");
	
		eng.dfs("eng.txt");
		rus.dfs("rus.txt");
		
	}
	
	public static String encode(String s){
		
		String result = new String();
		s = s.toUpperCase();
		
		for( int i = 0; i < s.length(); i++){
			
			Node temp = null;
			Tree tree = eng;
			char c =  s.charAt(i);
			
			if( c >= '�'&& c <= '�' ){
				tree = rus;
			}
			
			for( Node n: tree.list){
				if( n.value == c ){
					temp = n;
					break;
				}
			}
			
			while( temp.parent != null ){
				
				result += temp.b ? "-" : ".";
				temp = temp.parent;
				
			}
			result += " ";
			
		}
	
		return result;
	}

	public static String decode(String string, Node start) {
		
		String[] s = string.split(" ");
		String result = new String();
		
		
		
		for( String d : s){
			
			Node temp = start;
			for( int i = d.length() - 1; i >= 0; i-- ){	
				if( d.charAt(i) == '.' ){
					temp = temp.left;
				}else if( d.charAt(i) == '-' ){
					temp = temp.right;
				}else{
					return "Error";
				}
			}
			
			result += temp.value;
			
		}
	
		
		return result;
	}

	public static String decodeEng(String string) {
		return decode(string,eng.getRoot());
	}
	public static String decodeRus(String string) {
		return decode(string,rus.getRoot());
	}
	
	
	
	
	
}
