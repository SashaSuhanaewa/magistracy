package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controller.Controller;

public class View extends JFrame {


	private static final long serialVersionUID = -1611018474317579906L;

	public View() throws Exception{
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		Controller.load();
			
		JPanel butPanel = new JPanel();
		JPanel panel = new JPanel();
		
		final JTextField input = new JTextField();
		
		input.setPreferredSize(new Dimension(400,30));
		panel.add(input);
		getContentPane().add(panel,BorderLayout.CENTER);
		
		
		panel = new JPanel();

		JButton but = new JButton("������������");

		panel.add(but);
		butPanel.add(panel);
		
		
		but.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				input.setText(Controller.encode(input.getText()));
			}
		});
		

		but = new JButton("������������ (Eng)");
		panel = new JPanel();

		panel.add(but);
		butPanel.add(panel);
		
		but.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				input.setText(Controller.decodeEng(input.getText()));
			}
		});
		
			
		but = new JButton("������������ (Rus)");
		
		
		panel = new JPanel();
		panel.add(but);
		butPanel.add(panel);
		
		but.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				input.setText(Controller.decodeRus(input.getText()));
			}
		});
		
		butPanel.add(panel);

		
		
		getContentPane().add(butPanel,BorderLayout.PAGE_END);
		
		
		
		pack();
		setVisible(true);
		setLocation(500, 200);
		setResizable(false);
		
	}
	 
	
}
