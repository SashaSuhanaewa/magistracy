package Zadacha;

public class Quadrangle extends Figure {

	public Quadrangle() {
		x = new float[4];
		y = new float[4];
		SetType("��������������");
		perimeter = square = 0.0f;
		for (int i = 0; i < x.length; i++) {
			x[i] = 0;
			y[i] = 0;
		}
	}

	public Quadrangle(float x1[], float y1[]) {
		x = new float[4];
		y = new float[4];
		SetType("��������������");
		SetPos(x1, y1);

	}

	public float GetSquare() {
		return square;
	}

	public float GetPerimeter() {
		return perimeter;
	}

	public void SetPos(float x1[], float y1[]) {
		for (int i = 0; i < 4; i++) {
			x[i] = x1[i];
			y[i] = y1[i];
		}
		CalculateSquare();
		CalculatePerimeter();
		float xv[] = new float[4];
		float yv[] = new float[4];
		for (int i = 0; i < 4; i++) {
			xv[i] = x[i] - x[(i + 1) % 4];
			yv[i] = y[i] - y[(i + 1) % 4];
		}

		float a = (float) Math.sqrt((x[1] - x[0]) * (x[1] - x[0])
				+ (y[1] - y[0]) * (y[1] - y[0]));
		float b = (float) Math.sqrt((x[1] - x[2]) * (x[1] - x[2])
				+ (y[1] - y[2]) * (y[1] - y[2]));
		float c = (float) Math.sqrt((x[2] - x[3]) * (x[2] - x[3])
				+ (y[2] - y[3]) * (y[2] - y[3]));
		float d = (float) Math.sqrt((x[3] - x[0]) * (x[3] - x[0])
				+ (y[3] - y[0]) * (y[3] - y[0]));
		boolean trap = false;
		boolean paralel = false;
		boolean primoi = false;
		for (int i = 0; i < 4; i++) {
			float len = (float) Math.sqrt(xv[i] * xv[i] + yv[i] * yv[i]);
			for (int j = i + 1; j < 4; j++) {
				float len1 = (float) Math.sqrt(xv[j] * xv[j] + yv[j] * yv[j]);
				float cos = Math.abs((xv[i] * xv[j] + yv[i] * yv[j])
						/ (len * len1));
				if (cos == 0 && paralel)
					primoi = true;
				if (trap && cos == 1)
					paralel = true;
				if (cos == 1)
					trap = true;
			}
		}

		if (primoi && a == b && b == c && c == d)
			SetType("�������");
		else if (primoi)
			SetType("�������������");
		else if (paralel)
			SetType("�������������");
		else if (trap)
			SetType("��������");
		else
			SetType("��������������");

	}

	private void CalculateSquare() {
		float a = (float) Math.sqrt((x[1] - x[0]) * (x[1] - x[0])
				+ (y[1] - y[0]) * (y[1] - y[0]));
		float b = (float) Math.sqrt((x[1] - x[2]) * (x[1] - x[2])
				+ (y[1] - y[2]) * (y[1] - y[2]));
		float cd = (float) Math.sqrt((x[1] - x[3]) * (x[1] - x[3])
				+ (y[1] - y[3]) * (y[1] - y[3]));
		float c = (float) Math.sqrt((x[2] - x[3]) * (x[2] - x[3])
				+ (y[2] - y[3]) * (y[2] - y[3]));
		float d = (float) Math.sqrt((x[3] - x[0]) * (x[3] - x[0])
				+ (y[3] - y[0]) * (y[3] - y[0]));
		float p1 = (a + d + cd) / 2;
		float p2 = (b + c + cd) / 2;
		square = (float) Math.sqrt(p1 * (p1 - a) * (p1 - d) * (p1 - cd))
				+ (float) Math.sqrt(p2 * (p2 - b) * (p2 - c) * (p2 - cd));

	}

	private void CalculatePerimeter() {
		float a = (float) Math.sqrt((x[1] - x[0]) * (x[1] - x[0])
				+ (y[1] - y[0]) * (y[1] - y[0]));
		float b = (float) Math.sqrt((x[1] - x[2]) * (x[1] - x[2])
				+ (y[1] - y[2]) * (y[1] - y[2]));
		float c = (float) Math.sqrt((x[2] - x[3]) * (x[2] - x[3])
				+ (y[2] - y[3]) * (y[2] - y[3]));
		float d = (float) Math.sqrt((x[3] - x[0]) * (x[3] - x[0])
				+ (y[3] - y[0]) * (y[3] - y[0]));
		perimeter = a + b + c + d;
	}

	public String GetPos() {
		return "(" + x[0] + "," + y[0] + ")" + "(" + x[1] + "," + y[1] + ")"
				+ "(" + x[2] + "," + y[2] + ")" + "(" + x[3] + "," + y[3] + ")";
	}

	public String GetData() {
		String s = new String();
		s = s.concat(x.length + " ");
		for (int i = 0; i < x.length; i++) {
			s = s.concat(((int) x[i]) + " " + ((int) y[i]) + " ");
		}
		return s;
	}

}
