package Zadacha;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;


public class Controller {
	ArrayList<Figure> figures;
	public Controller() {
		figures = new ArrayList<Figure>();
	}

	public void add(int count, float x[], float y[]) {
		if (count == 3)
			addTriangle(x, y);
		else if (count == 4)
			addQuadrangle(x, y);
	}

	public void addTriangle(float x[], float y[]) {
		figures.add(new Triangle(x, y));
	}

	public void addQuadrangle(float x[], float y[]) {
		figures.add(new Quadrangle(x, y));
	}

	public void remove(int a) {
		figures.remove(a);
	}

	public int getCout() {
		return figures.size();
	}
	
	public String getInfo(int a) {
		if (a > figures.size())
			return "Error";
		Figure f = figures.get(a);
		return a + ": " + f.GetType() + " " + f.GetPos() + " �������: "+f.GetSquare()
				+ " ��������: " + f.GetPerimeter();
	}

	public void loadData() {
		figures.clear();
		File f = new File("DataLoad.txt");
		Scanner sc = null;
		try {
			sc = new Scanner(f);
		} catch (FileNotFoundException e) {
			System.out.println("���� �� ������!!!!!");
		}
		while (sc.hasNext()) {
			int k = sc.nextInt();
			float x[] = new float[k];
			float y[] = new float[k];
			for (int i = 0; i < k; i++) {
				x[i] = sc.nextFloat();
				y[i] = sc.nextFloat();
			}
			this.add(k, x, y);
		}
		sc.close();
	}

	public void saveData() {
		File f = new File("DataSave.txt");
		if(!f.exists())
			try {
				f.createNewFile();
			} catch (IOException e1) {
				e1.printStackTrace();
			} 
		PrintWriter out = null;
		try {
			out = new PrintWriter(f);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		for( int i = 0; i < figures.size(); i++){
			out.print(figures.get(i).GetData() + "\n");
		}
		out.close();
	}
	
	public float area(float ax, float bx, float cx,float ay,float by,float cy){
		return (bx-ax)*(cy - ay)-(by - ay)*(cx - ax);
	}
	public boolean intersect_1	(float a,float b,float c,float d){
		if (a > b){
			float temp=b;
			b=a;
			a=temp;
		}
		if (c > d){
			float temp=d;
			d=c;
			c=temp;
		}
		return Math.max(a,c)<=Math.min(b,d);
	}
	public boolean inter(float ax, float bx, float cx,float dx,float ay,float by,float cy,float dy){
		boolean v=intersect_1 (ax, bx, cx, dx)
				&& intersect_1 (ay,by,cy,dy)
				&& area(ax,bx,cx,ay,by,cy) * area(ax,bx,dx,ay,by,dy) <= 0
				&& area(cx,dx,ax,cy,dy,ay) * area(cx,dx,bx,cy,dy,by) <= 0;
		
		return v;	}
	
	public void cross(int a,int b){
		Figure f = figures.get(a);
		float x[]=f.x;
		float y[]=f.y;
		Figure f1 = figures.get(b);
		float x1[]=f1.x;
		float y1[]=f1.y;
		boolean temp=false;
		for(int i=1;i<x.length;i++){
			for(int j=1;j<x1.length;j++){
				temp=inter(x[i-1],x[i],x1[j-1],x1[j],y[i-1],y[i],y1[j-1],y1[j]);
				if(temp==true)
					break;
			}
			if(temp==true)
				break;
			}
		if (temp==true)
			System.out.println("������ ������������ ");
		else
			System.out.println("������ �� ������������ ");
	}
/*	public float max(float a[]){
		float max=a[0];
		for(int i=1;i<a.length;i++)
			if(a[i]>max){
				max=a[i];
			}
		return max;
	}
	public float min(float a[]){
		float min=a[0];
		for(int i=1;i<a.length;i++)
			if(a[i]<min){
				min=a[i];
			}
		return min;
	}
	public void cross(int a,int b){
		Figure f = figures.get(a);
		float x[]=f.x;
		float y[]=f.y;
		Figure f1 = figures.get(b);
		float x1[]=f1.x;
		float y1[]=f1.y;
		float maxX=max(x);
		float minX=min(x);
		float maxY=max(y);
		float minY=min(y);
		float maxX1=max(x1);
		float minX1=min(x1);
		float maxY1=max(y1);
		float minY1=min(y1);
		boolean d=false;
		if((((minX<=minX1 && minX1<=maxX) || (minX<=maxX1 && maxX1<=maxX))
				&&((minY<=minY1 && minY1<=maxY) ||(minY<=maxY1 && maxY1<=maxY)))
				&&
				(((minX1<=minX && minX<=maxX1)||(minX1<=maxX && maxX<=maxX1))&&
				((minY1<=minY && minY<=maxY1) ||(minY1<=maxY && maxY<=maxY1))))
				d=true;
		if((((minX<minX1 && minX1<maxX) && (minX<maxX1 && maxX1<maxX))
				&&((minY<minY1 && minY1<maxY)&&(minY<maxY1 && maxY1<maxY)))
				||
				(((minX1<minX && minX<maxX1)&&(minX1<maxX && maxX<maxX1))&&
				((minY1<minY && minY<maxY1) &&(minY1<maxY && maxY<maxY1))))
				d=false;
		if(d)
			System.out.println("������ ������������");
		else
			System.out.println("������ �� ������������");
		
	}*/
		
	/*public void cross(int a,int b){
		Figure f = figures.get(a);
		float x[]=f.x;
		float y[]=f.y;
		Figure f1 = figures.get(b);
		float x1[]=f1.x;
		float y1[]=f1.y;
		boolean temp = false;
		boolean t=false;
		for (int i = 0; i < x.length - 1; i++) {
			if (temp==true)
				break;
			for (int j = 0; j < x1.length - 1; j++) {
				float urx = (-x1[j] * y1[j + 1] + x1[j] * y1[j] + y1[j]
						+ x[i] * y[i + 1] + y[i] * x[i] + y[i])
						/ (y[i + 1] - y[i] - y1[j + 1] + y1[j]);
				if (((x1[j] >= urx) && (urx >= x1[j + 1])) || ((x1[j] <= urx)
						&& (urx <= x1[j + 1])))				
					temp = true;
				float ury = ((x1[j] - x[i])*(y[i+1] - y[i])*(y1[j+1] - y1[j]) - 
						(x[i]*y[i] - x[i+1]*y[i])*(y1[j+1]-y1[j]) - 
						(x1[j+1]*y1[j]-x1[j]*y[i])*(y[i+1]-y[i]))/
						((x[i+1]-x[i])*(y[i+1]-y[i]) - (y[i+1]-y[i])*(x1[j+1]-x1[j]) );
				if ((y[j] >= ury) && (ury >= y[j + 1]) || (y[j] <= ury)
				&& (ury <= y[j + 1]))
					temp=true;
				
				if(x[i]==x1[j] && y[i]==y1[j] )
					 t=true;
				if (temp||t)
					break;
				}
			}
		if (temp == false)
		System.out.println("������ ������������");
			else
				if (t == false)
					System.out.println("������ �� ������������");
					else
					System.out.println("������ ������������");
		}
*/
	
		
	}

