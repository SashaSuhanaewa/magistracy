package Zadacha;

public class Triangle extends Figure {

	public Triangle() {
		x = new float[3];
		y = new float[3];
		SetType("�����������");
		perimeter = square = 0.0f;
		for (int i = 0; i < x.length; i++) {
			x[i] = 0;
			y[i] = 0;
		}
	}

	public Triangle(float x1[], float y1[]) {
		x = new float[3];
		y = new float[3];
		SetType("�����������");
		SetPos(x1, y1);

	}
	public float GetSquare() {
		return square;
	}

	public float GetPerimeter() {
		return perimeter;
	}

	public void SetPos(float x1[], float y1[]) {
		for (int i = 0; i < 3; i++) {
			x[i] = x1[i];
			y[i] = y1[i];
		}
		CalculatePerimeter();
		CalculateSquare();		
	}
	
	private void CalculatePerimeter() {
		float a = (float) Math.sqrt((x[1] - x[0]) * (x[1] - x[0])
				+ (y[1] - y[0]) * (y[1] - y[0]));
		float b = (float) Math.sqrt((x[1] - x[2]) * (x[1] - x[2])
				+ (y[1] - y[2]) * (y[1] - y[2]));
		float c = (float) Math.sqrt((x[2] - x[0]) * (x[2] - x[0])
				+ (y[2] - y[0]) * (y[2] - y[0]));
		perimeter = a + b + c;
	
	}
	private void CalculateSquare(){
		float a = (float) Math.sqrt((x[1] - x[0]) * (x[1] - x[0])
				+ (y[1] - y[0]) * (y[1] - y[0]));
		float b = (float) Math.sqrt((x[1] - x[2]) * (x[1] - x[2])
				+ (y[1] - y[2]) * (y[1] - y[2]));
		float c = (float) Math.sqrt((x[2] - x[0]) * (x[2] - x[0])
				+ (y[2] - y[0]) * (y[2] - y[0]));
		float p=(a+b+c)/2;
		square=(float) Math.sqrt(p*(p-a)*(p-b)*(p-c));
	}

	public String GetPos() {
		return "(" + x[0] + "," + y[0] + ")" + "(" + x[1] + "," + y[1] + ")"
				+ "(" + x[2] + "," + y[2] + ")";
	}

	public String GetData() {
		String s = new String();
		s = s.concat(x.length + " ");
		for (int i = 0; i < x.length; i++) {
			s = s.concat(((int)x[i]) + " " + ((int)y[i]) + " ");
		}
		return s;
	}

}
