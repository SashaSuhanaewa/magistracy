package Zadacha;

import java.util.Scanner;
public class View {
	static Scanner sc = new Scanner(System.in);
	static Controller controller;

	public void view() {
		controller = new Controller();
		while (mainMenu());
		sc.close();	
	}

	public void writeMenu(String m[]) {
		writeMenu(m, "�����");
	}

	public void writeMenu(String m[], String exit) {
		for (int i = 0;i < m.length; i++) {
			System.out.println(i+1 + "." + m[i]);
		}
		System.out.println("0." + exit);
	}

	public boolean mainMenu() {
		String m[] = { "�������� ������.", "������� ������.",
				"����� id ���� �����, �� ��������� � ��� ������.",
				"������� ������ �� �����", "��������� ������",
				"����������� �����"};
		writeMenu(m);
		int a=sc.nextInt();
		if (a==0)
			return false;
		switch (a) {
		case 1:
			addMenu();
			break;
		case 2:
			removeMenu();
			break;
		case 3:
			showMenu();
			break;
		case 4:
			loadMenu();
			break;
		case 5:
			saveMenu();
			break;
		case 6:
			crossing();
			break;
		}
		
		return true;
	}
	public void loadMenu() {				//����������
		controller.loadData();
	}
	public void saveMenu() {				//��������
		controller.saveData();
	}
	public void addMenu() {					//�������
		String m[] = { "���������� ����� � �� ����������",
				"��� ������ � ����������" };
		writeMenu(m, "������");
		int t = sc.nextInt();
		float x[] = new float[4];
		float y[] = new float[4];
		int a = 0;
		switch (t) {
		case 1:
			a = sc.nextInt();
			if (a < 3)
				break;
			break;
		case 2:
			String m1[] = { "�����������", "��������������" };
			writeMenu(m1, "������");
			a = sc.nextInt();
			if (a == 1)
				a = 3;
			else if (a == 2)
				a = 4;
			else
				break;

		}

		for (int i = 0; i < a; i++) {
			x[i] = sc.nextFloat();
			y[i] = sc.nextFloat();
		}
		controller.add(a, x, y);

	}

	public void removeMenu() {                 //��������
		System.out.print("-1.������\n������� ID ������: ");
		int a = sc.nextInt();
		if (a == -1)
			return;
		if(a < -1)
			System.out.println("Error!!!");
		controller.remove(a);
	}

	public void showMenu() {                    //����
		System.out.println("ID Type Pos Square Perimeter");
		for (int i = 0; i < controller.getCout(); i++) {
			System.out.println(controller.getInfo(i));
		}
		System.out.print("\n");
	}
	public void crossing(){						//�����������
		System.out.print("-1.������\n������� ID �����: \n");
		int a=sc.nextInt();
		int b;
		if (a == -1)
			return;
		else{
			b=sc.nextInt();
			if(a<-1 || b<-1)
				System.out.println("Error!!!");
		}
		controller.cross(a, b);
	}
}
