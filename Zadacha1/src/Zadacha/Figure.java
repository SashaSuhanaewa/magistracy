package Zadacha;

abstract public class Figure {

	protected float square;
	protected float perimeter;
	private String type;
	protected float x[];
	protected float y[];

	public void SetType(String a) {
		type = new String(a);
	}

	public String GetType() {
		return type;
	}

	abstract public void SetPos(float x1[], float y1[]);

	abstract public float GetSquare();

	abstract public float GetPerimeter();

	abstract public String GetPos();
	abstract public String GetData();
}
